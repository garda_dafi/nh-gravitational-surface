#include "param.cu"
#include "sub.cu"
#include "../cpuSecond.h"
int main(){
  int __attribute__((unused)) n, nout;
  int ntotal; //i, j, idx;
  double wl, ps;

  init_param_host();
  init_param_device();
  wl = G*period*period/(2.*PI);
  printf("Depp-water wavelength (m) is %f\n", wl);
  ps = wl/period;
  printf("Deep-water phase speed (m/s) is %f\n", ps);

//////////////////////////////////////////////////////////////////////////////////////
// init data
  dim3 gridWet(nz+1);
  dim3 blockWet(nx+1);
  initWet<<<gridWet,blockWet>>>(d_wet);

  dim3 gridWetFalse(1);
  dim3 blockWetFalse(nz+1);
  initWetFalse<<<gridWetFalse,blockWetFalse>>>(d_wet);

  dim3 gridBottomTopograhy(1);
  dim3 blockBottomTopograhy(nx-1);
  initBottomTopography<<<gridBottomTopograhy, blockBottomTopograhy>>>(d_depth);

  dim3 gridParamSOR(nz-1);
  dim3 blockParamSOR(nx);
  initParamSOR<<<gridParamSOR, blockParamSOR>>>(d_ct, d_cb, d_ce, d_cw, d_ctot, d_wet);

  // Synchronize init data
  cudaDeviceSynchronize();
//////////////////////////////////////////////////////////////////////////////////////

  // runtime parameters
  ntotal = (int)(100./dt);
  printf("ntotal %d\n", ntotal);
  double time = 0.0;

  // output parameters
  nout = (int)(1/dt);

  // FILE *dp_file, *u_file, *w_file,
  FILE *eta_file;
  // dp_file = fopen("dp.dat", "w+");
  // u_file = fopen("u.dat", "w+");
  // w_file = fopen("w.dat", "w+");
  eta_file = fopen("eta.dat", "w+");

  // simulation loop
  // ntotal = 1;

  HANDLE_ERROR(cudaMemcpy(h_wet, d_wet, nxz*sizeof(h_wet[0]), cudaMemcpyDeviceToHost));
  double iStart = cpuSecond();
  for(n=1; n<=ntotal; n++){
    time += dt;

    // variation of forcing
    h_ad = amplitude*sin(2.*PI*time/period);
    cudaMemcpyToSymbol(d_ad, &h_ad, sizeof(double), 0, cudaMemcpyHostToDevice);
    printf("ad %i\t=> %0.8e  |\n",n, h_ad);

    // prognostic equations
    dyn();

    // save data to file
    // if((n%nout) == 0){
    // if(n == 1){
    //   printf("Data output at time = %f\n", time/(24.*3600.));
    //   for(i = 1; i< nz; i++){
    //     for(j = 1; j < nx; j++){
    //       idx = j + i*(nx+1);
    //       fprintf(dp_file, "%f\n", h_dp[idx]/(RHO*G));
    //       fprintf(u_file, "%f\n", h_u[idx]);
    //       fprintf(w_file, "%f\n", h_w[idx]);
    //     }
    //   }
      // HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(h_dp[0]), cudaMemcpyDeviceToHost));
      // for(int i= 1; i<nx; i++){
      //   fprintf(eta_file, "%f\n", h_dp[i]/(RHO*G));
      // }
    // }
  }
  double iElaps = cpuSecond() - iStart;
  printf("Time elapsed %0.17f s\n", iElaps);

  // fclose(dp_file);
  // fclose(u_file);
  // fclose(w_file);
  fclose(eta_file);
  free(h_wet);      cudaFree(d_wet);
  free(h_dp);       cudaFree(d_dp);
  free(h_u);        cudaFree(d_u);
  free(h_un);       cudaFree(d_un);
  free(h_ustar);    cudaFree(d_ustar);
  free(h_depth);    cudaFree(d_depth);
  free(h_w);        cudaFree(d_w);
  free(h_wn);       cudaFree(d_wn);
  free(h_wstar);    cudaFree(d_wstar);
  free(h_pstar);    cudaFree(d_pstar);
  free(h_q);        cudaFree(d_q);
  free(h_ct);       cudaFree(d_ct);
  free(h_cb);       cudaFree(d_cb);
  free(h_cw);       cudaFree(d_cw);
  free(h_ce);       cudaFree(d_ce);
  free(h_ctot);     cudaFree(d_ctot);
  free(h_dpstore);  cudaFree(d_dpstore);
  return 0;
}
