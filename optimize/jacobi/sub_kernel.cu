// init
__global__ void initWet(int *wet){
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  wet[i] = 1;
}

__global__ void initWetFalse(int *wet){
    int lBound    = threadIdx.x * (d_nx+1);
    int rBound    = lBound + d_nx;
    wet[lBound]   = 0;
    wet[rBound]   = 0;
}

__global__ void initBottomTopography(double* depth){
  if(threadIdx.x < 51){
    depth[threadIdx.x+1] = 100.0-95.0*(double)(threadIdx.x+1)/(double)51;
  }else{
    depth[threadIdx.x+1] = 5.0+95.0*(double)((threadIdx.x+1)-51)/(double)((d_nx-1)-51);
  }
}

__global__ void initWetAndDryPointer(double *depth, int *wet){
  int idx = threadIdx.x + blockIdx.x * blockDim.x;
  d_nb = (int)(depth[idx]/d_dz);
  wet[idx] = false;
}

__global__ void initParamSOR(double *ct, double *cb, double *ce, double *cw, double *ctot, int *wet){
    int idx      = threadIdx.x + (blockIdx.x+1) * (blockDim.x+1);
    int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (blockDim.x+1);
    int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (blockDim.x+1);
    int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (blockDim.x+1);
    int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (blockDim.x+1);

    int lBound = (blockIdx.x+1) * (blockDim.x+1);
    int rBound = lBound + (blockDim.x);

    if((idx != lBound) && (idx != rBound)){
      ct[idx] = (double)(d_dx/d_dz);
      cb[idx] = (double)(d_dx/d_dz);
      ce[idx] = (double)(d_dz/d_dx);
      cw[idx] = (double)(d_dz/d_dx);
      if(!wet[idxZmin]){
        ct[idx] = 0.0;
      }
      if(!wet[idxZplus]){
        cb[idx] = 0.0;
      }
      if(!wet[idxXplus]){
        ce[idx] = 0.0;
      }
      if(!wet[idxXmin]){
        cw[idx] = 0.0;
      }
      ctot[idx] = ct[idx] + cb[idx] + ce[idx] + cw[idx];
    }
}


// dyn
// S.O.R Algorithm
__global__ void surfacePressureField(double *dpstore, double *dp){
    dpstore[threadIdx.x] = dp[threadIdx.x];
}

__global__ void calculateUstarAndWstar(double *ustar, double *u, double *wstar, double *w, double *dp, int *wet){
  d_drdxh = 0.5/(d_RHO*d_dx);
  d_drdzh = 0.5/(d_RHO*d_dz);
  int idx      = threadIdx.x + (blockIdx.x+1) * (blockDim.x+1);
  int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (blockDim.x+1);
  int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (blockDim.x+1);


  int lBound = (blockIdx.x+1) * (blockDim.x+1);
  int rBound = lBound + (blockDim.x-1);

  if((idx != lBound) && (idx != rBound)){
    if(wet[idx]){
      d_pressx = -d_drdxh*(dp[idxXplus] - dp[idx]);
      if(wet[idxXplus]) {
        ustar[idx] = u[idx] + d_dt*d_pressx;
      }
      d_pressz = -d_drdzh*(dp[idxZmin] - dp[idx]);
      if(wet[idxZmin]){
        wstar[idx] = w[idx] + d_dt*d_pressz;
      }
    }
  }
}
__global__ void calculatePoisson(double *pstar, double *ustar, double *u, double *wstar, double *w){
  int idx      = threadIdx.x + (blockIdx.x+1) * (blockDim.x+1);
  int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (blockDim.x+1);
  int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (blockDim.x+1);

  int lBound = (blockIdx.x+1) * (blockDim.x+1);
  int rBound = lBound + (blockDim.x);

  if((idx != lBound) && (idx != rBound)){
    pstar[idx] = -2.0*d_RHO/d_dt * (\
      (ustar[idx] - u[idx] - ustar[idxXmin] + u[idxXmin]) * d_dz + \
      (wstar[idx] - w[idx] - wstar[idxZplus] + w[idxZplus]) * d_dx);
  }

}
__global__ void algorithmJacobi(){
  int idx      = threadIdx.x + (blockIdx.x+1) * (blockDim.x+1);
  int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (blockDim.x+1);
  int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (blockDim.x+1);
  int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (blockDim.x+1);
  int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (blockDim.x+1);

  int lBound = (blockIdx.x+1) * (blockDim.x+1);
  int rBound = lBound + (blockDim.x);

  if((idx != lBound) && (idx != rBound)){
    d_p1 = dp[idx];
    d_p2 = 1.0/ctot[idx]*(ce[idx] * dp[idxXplus] + cw[idx] * dp[idxXmin] + ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus]);
  }
}
__global__ void algorithmSOR(
  double *ct    , double *cb      , double *ce , double *cw, double *ctot,
  double *pstar , double *dp      , int *wet
){
  int i        , j       , idx      , lBound, rBound,
      idxZplus , idxZmin , idxXplus , idxXmin;

  d_nstop = 1000;
  for(d_nsor = 1; d_nsor <= d_nstop; d_nsor++){
    d_perr = 0.0;
    // Step 4.1: predict new pressure
    for(i = 0; i<=(d_nz-2); i++){
      for(j = 0; j<=d_nx; j++){
        idx = j + (i+1) * (d_nx+1);
        lBound = i * (d_nx+1);
        rBound = lBound + d_nx;
        if((idx != lBound) && (idx != rBound)){
          idxZplus = j + ((i+1)+1) * (d_nx+1);
          idxZmin  = j + ((i+1)-1) * (d_nx+1);
          idxXplus = (j+1) + (i+1) * (d_nx+1);
          idxXmin  = (j-1) + (i+1) * (d_nx+1);
          if(wet[idx]){
            d_p1 = dp[idx];
            d_term1 = pstar[idx] + \
            (ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus]) + \
            (cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus]);
            d_p2 = (1.0 - d_omega) * d_p1 + d_omega * d_term1 / ctot[idx];
            dp[idx] = d_p2;
            // d_perr = fmax(fabs(d_p2 - d_p1), d_perr);
          } // endif
        }
      }
    }
    for(i=0; i<=(d_nz-2); i++){
      lBound = (i+1)*(d_nx+1);
      rBound = lBound + d_nx;
      dp[lBound] = dp[lBound + 1];
      dp[rBound] = dp[rBound - 1];
    }
    if(d_perr <= d_peps){
      d_nstop = d_nsor;
      d_perr = 0;
    }
  }// end nsor
}

__global__ void predictNewVelocity(double *un, double *ustar, double *wn, double *wstar, double *dp, int *wet){
  int idx    = threadIdx.x + (blockIdx.x+1) * (blockDim.x+1);
  int lBound = blockIdx.x * (blockDim.x+1);
  int rBound = lBound + blockDim.x;

  if((idx != lBound) && (idx != rBound)){
    int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (blockDim.x+1);
    int idxXplus = (threadIdx.x+1) + (blockIdx.x+1) * (blockDim.x+1);
    if(wet[idx]){
      d_pressx = -d_drdxh*(dp[idxXplus] - dp[idx]);
      if(wet[idxXplus]){
        un[idx] = ustar[idx] + d_dt * d_pressx;
        d_pressz = -d_drdzh * (dp[idxZmin] - dp[idx]);
      }
      if(wet[idxZmin]){
        wn[idx] = wstar[idx] + d_dt*d_pressz;
      } // endif
    }
  }
}

__global__ void predictDepthIntegratedFlow(double *q, double *un){
  for(int j = 1; j<=d_nz; j++){
    int idx = threadIdx.x + j * (d_nx+1);
    q[threadIdx.x] += d_dz * un[idx];
  }
}

__global__ void predictSurfacePressureField(double *dp, double *dpstore, double *q){
  // for(i = 1;i<=d_nx; i++){
    dp[(threadIdx.x+1)] = dpstore[(threadIdx.x+1)] - d_dt * d_RHO * d_G * (q[(threadIdx.x+1)] - q[(threadIdx.x+1)-1]) / d_dx;
  // }
}
__global__ void lateralBoundaryConditionQ(double *q){
    q[0] = 0.0;
    q[d_nx] = 0.0;
    q[d_nx+1] = q[d_nx];
}
__global__ void lateralBoundaryCondition(double *u){
  int idx = threadIdx.x+1 + blockIdx.x+1 * blockDim.x;
  u[threadIdx.x] = 0.0;
  u[idx] = 0.0;
  u[idx] = 0.0;
}

__global__ void updatingNextTimeStep(double *u, double *un, double *w, double *wn){
  int idx = threadIdx.x + (blockIdx.x+1) * (blockDim.x+1);
  u[idx]  = un[idx];
  w[idx]  = wn[idx];
}
