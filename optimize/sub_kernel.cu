// init
__global__ void initWet(int *wet){
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  wet[i] = 1;
}

__global__ void initWetFalse(int *wet){
    int lBound    = blockIdx.x * (d_nx+1);
    int rBound    = lBound + d_nx;
    wet[lBound]   = 0;
    wet[rBound]   = 0;
}

__global__ void initBottomTopography(double* depth){
  if(threadIdx.x <= 50){
    depth[threadIdx.x+1] = 100.0-95.0*(double)(threadIdx.x+1)/(double)51;
  }else{
    depth[threadIdx.x+1] = 5.0+95.0*(double)((threadIdx.x+1)-51)/(double)(d_nx-51);
  }
}

__global__ void initWetAndDryPointer(double *depth, int *wet){
  for(int i=0; i<nz-1; i++){
    int nb = (int)(depth[i+1]/dz);
    for(int j=(nb+30); j< nx; j++){
      int idx = j + (i+1)*(nx+1);
        wet[idx] = false;
    }
  }
}

__global__ void initParamSOR(double *ct, double *cb, double *ce, double *cw, double *ctot, int *wet){
    int idx      = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
    int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (d_nx+1);
    int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
    int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (d_nx+1);
    int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (d_nx+1);

    int lBound = (blockIdx.x+1) * (d_nx+1);
    int rBound = lBound + d_nx;

    if((idx != lBound) && (idx != rBound)){
      ct[idx] = (double)(d_dx/d_dz);
      cb[idx] = (double)(d_dx/d_dz);
      ce[idx] = (double)(d_dz/d_dx);
      cw[idx] = (double)(d_dz/d_dx);
      if(!wet[idxZmin]){
        ct[idx] = 0.0;
      }
      if(!wet[idxZplus]){
        cb[idx] = 0.0;
      }
      if(!wet[idxXplus]){
        ce[idx] = 0.0;
      }
      if(!wet[idxXmin]){
        cw[idx] = 0.0;
      }
      ctot[idx] = ct[idx] + cb[idx] + ce[idx] + cw[idx];
    }
}

// dyn
// S.O.R Algorithm
__global__ void surfacePressureField(double *dpstore, double *dp){
    dpstore[blockIdx.x] = dp[blockIdx.x];
}

__global__ void calculateUstarAndWstar(double *ustar, double *u, double *wstar, double *w, double *dp, int *wet){
  d_drdxh = 0.5/(d_RHO*d_dx);
  d_drdzh = 0.5/(d_RHO*d_dz);
  int idx      = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
  int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (d_nx+1);


  int lBound = (blockIdx.x+1) * (d_nx+1);
  int rBound = lBound + blockDim.x;

  if((idx != lBound) && (idx != rBound)){
    if(wet[idx]){
      double pressx = -d_drdxh*(dp[idxXplus] - dp[idx]);
      if(wet[idxXplus]) {
        ustar[idx] = u[idx] + d_dt*pressx;
      }
      double pressz = -d_drdzh*(dp[idxZmin] - dp[idx]);
      if(wet[idxZmin]){
        wstar[idx] = w[idx] + d_dt*pressz;
      }
    }
  }

}
__global__ void calculatePoisson(double *pstar, double *ustar, double *u, double *wstar, double *w){
  int idx      = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (d_nx+1);
  int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (d_nx+1);

  int lBound = (blockIdx.x+1) * (d_nx+1);
  int rBound = lBound + d_nx;

  if((idx != lBound) && (idx != rBound)){
    pstar[idx] = -2.0*d_RHO/d_dt * \
      ((ustar[idx] - u[idx] - ustar[idxXmin] + u[idxXmin]) * d_dz + \
      (wstar[idx] - w[idx] - wstar[idxZplus] + w[idxZplus]) * d_dx);
  }
}

__global__ void predictNewVelocity(double *un, double *ustar, double *wn, double *wstar, double *dp, int *wet){
  int idx    = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  int lBound = (blockIdx.x+1) * (d_nx+1);
  int rBound = lBound + d_nx;
  int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
  int idxXplus = (threadIdx.x+1) + (blockIdx.x+1) * (d_nx+1);
  double pressz, pressx;

  if((idx != lBound) && (idx != rBound)){
    if(wet[idx]){
      pressx = -d_drdxh*(dp[idxXplus] - dp[idx]);
      pressz = -d_drdzh*(dp[idxZmin] - dp[idx]);
      if(wet[idxXplus]){
        un[idx] = ustar[idx] + d_dt * pressx;
      }
      if(wet[idxZmin]){
        wn[idx] = wstar[idx] + d_dt*pressz;
      }
    }
  }
}

__global__ void predictDepthIntegratedFlow(double *q, double *un){
  q[blockIdx.x] = 0.0;
  for(int j = 0; j<nz-1; j++){
    int idx  = blockIdx.x + (j+1)*(nx+1);
    q[blockIdx.x] = q[blockIdx.x] + d_dz*un[idx];
  }
}

__global__ void predictSurfacePressureField(double *dp, double *dpstore, double *q){
  dp[blockIdx.x+1] = dpstore[blockIdx.x+1] - d_dt * d_RHO * d_G * (q[(blockIdx.x+1)] - q[blockIdx.x]) / d_dx;
}

__global__ void lateralBoundaryConditionQ(double *q){
    q[0] = 0.0;
    q[d_nx+1] = 0.0;
    q[d_nx] = q[d_nx-1];
}

__global__ void shared_SOR(
  double *ct, double *cb, double *ce, double *cw, double *ctot,
  double *pstar, double *dp, int *wet, double *error, double *temp){
  int i, j, idx, lBound, rBound;
  int idxZplus, idxZmin, idxXplus, idxXmin;
  double p1, p2, term1;

  // __shared__ double s_dp[1];
  idx    = threadIdx.x + (blockIdx.x+1)*(d_nx+1);
  lBound = (blockIdx.x+1) * (d_nx+1);
  rBound = lBound + d_nx;

  if((idx != lBound) && (idx != rBound)){
    idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (d_nx+1);
    idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
    idxXplus = (threadIdx.x+1) + (blockIdx.x+1) * (d_nx+1);
    idxXmin  = (threadIdx.x-1) + (blockIdx.x+1) * (d_nx+1);
    // s_dp[0] = dp[idx];
    //
    // __syncthreads();

    if(wet[idx]){
      error[idx] = 0.0;
      p1 = dp[idx];
      term1 = pstar[idx] + \
      (ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus]) + \
      (cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus]);
      p2 = (1.0 - d_omega) * p1 + d_omega * term1 / ctot[idx];
      temp[idx] = p2;
      error[idx] = fabs(p2 - p1);
      // printf("%i %f & %f\n",idx, (dp[idxZmin]),  dp[idxZplus]);
    } // endif

    // __syncthreads();
    // dp[idx] = s_dp[0];
    // printf("%f\n", pstar[idx]);
    // dp[idx] = p2;
  }
}
__global__ void temp_SOR(int *wet, double *dp, double *temp){
  int i, j, idx, lBound, rBound;
  int idxZplus, idxZmin, idxXplus, idxXmin;

  idx    = threadIdx.x + (blockIdx.x+1)*(d_nx+1);
  lBound = (blockIdx.x+1) * (d_nx+1);
  rBound = lBound + d_nx;

  if((idx != lBound) && (idx != rBound)){
    if(wet[idx]){
      dp[idx] = temp[idx];
    }
  }
}

__global__ void boundaryLeftRight(double *dp){
  int lBound, rBound;
  lBound = (blockIdx.x+1)*(d_nx+1);
  rBound = lBound + d_nx;
  dp[lBound] = dp[lBound + 1];
  dp[rBound] = dp[rBound - 1];
}



__global__ void algorithmSOR_Shared(
  double *ct, double *cb, double *ce, double *cw, double *ctot,
  double *pstar, double *dp, int *wet
){
  int i, j, idx, lBound, rBound;
  int idxZplus, idxZmin, idxXplus, idxXmin;
  float p1, p2, term1;
  __shared__ float s_dp[6000];
  __shared__ double perr[1];

  for(int i=0;i<=nz;i++){
    for(int j=1; j<=nx+1; j++){
      int idx = (j-1)+i*(nx+1);
      s_dp[idx] = dp[idx];
    }
  }
  perr[0] = 0.0;
  idx    = threadIdx.x + (blockIdx.x+1)*(nx+1);
  lBound = (blockIdx.x+1) * (nx+1);
  rBound = lBound + nx;
  if((idx != lBound) && (idx != rBound)){
    idxZplus = threadIdx.x + ((i+1)+1) * (nx+1);
    idxZmin  = threadIdx.x + ((i+1)-1) * (nx+1);
    idxXplus = (threadIdx.x+1) + (i+1) * (nx+1);
    idxXmin  = (threadIdx.x-1) + (i+1) * (nx+1);
    if(wet[idx]){
      // p1 = s_dp[idx];
      atomicExch(&p1, s_dp[idx]);
      term1 = pstar[idx] + \
      (ct[idx] * s_dp[idxZmin] + cb[idx] * s_dp[idxZplus]) + \
      (cw[idx] * s_dp[idxXmin] + ce[idx] * s_dp[idxXplus]);
      p2 = (1.0 - d_omega) * p1 + d_omega * term1 / ctot[idx];
      atomicExch(&s_dp[idx], p2);
      // s_dp[idx] = p2;
      perr[0] = fmax(fabs(p2 - p1), perr[0]);
    } // endif
  }

  for(int i=0;i<=nz;i++){
    for(int j=1; j<=nx+1; j++){
      int idx = (j-1)+i*(nx+1);
      dp[idx] = s_dp[idx];
    }
  }
}
// __global__ void algorithmSOR(
//   double *ct, double *cb, double *ce, double *cw, double *ctot,
//   double *pstar, double *dpstore, double *dp, double *ustar,
//   double *un, double *wstar, double *wn, double *q, int *wet
// ){
//   int i, j, idx, lBound, rBound;
//   int idxZplus, idxZmin, idxXplus, idxXmin;
//   double perr = 0.0;
//   double p1,p2, term1;
//   __shared__ double s_dp[6000];
//
//   for(int i=0;i<=nz;i++){
//     for(int j=1; j<=nx+1; j++){
//       int idx = (j-1)+i*(nx+1);
//       s_dp[idx] = dp[idx];
//     }
//   }
//   // Step 4.1: predict new pressure
//
//   int nstop = 10;
//   for(int nsor = 1; nsor <= nstop; nsor++){
//     perr = 0.0;
//     for(i = 0; i<nz-1; i++){
//       for(j = 0; j<nx; j++){
//         idx    = j + (i+1)*(nx+1);
//         lBound = (i+1) * (nx+1);
//         rBound = lBound + nx;
//         if((idx != lBound) && (idx != rBound)){
//           idxZplus = j + ((i+1)+1) * (nx+1);
//           idxZmin  = j + ((i+1)-1) * (nx+1);
//           idxXplus = (j+1) + (i+1) * (nx+1);
//           idxXmin  = (j-1) + (i+1) * (nx+1);
//           if(wet[idx]){
//             p1 = s_dp[idx];
//             term1 = pstar[idx] + \
//             (ct[idx] * s_dp[idxZmin] + cb[idx] * s_dp[idxZplus]) + \
//             (cw[idx] * s_dp[idxXmin] + ce[idx] * s_dp[idxXplus]);
//             p2 = (1.0 - d_omega) * p1 + d_omega * term1 / ctot[idx];
//             s_dp[idx] = p2;
//             perr = fmax(fabs(p2 - p1), perr);
//           } // endif
//         }
//       }
//     } // endfor dp
//
//     for(i=0; i<nz-1; i++){
//       lBound = (i+1)*(nx+1);
//       rBound = lBound + nx;
//       s_dp[lBound] = s_dp[lBound + 1];
//       s_dp[rBound] = s_dp[rBound - 1];
//     }
//     for(int i=0;i<=nz;i++){
//       for(int j=1; j<=nx+1; j++){
//         int idx = (j-1)+i*(nx+1);
//         dp[idx] = s_dp[idx];
//       }
//     }
//     // for(int i=0; i<nx+1; i++){
//     //   printf("%0.7e\n", dpstore[i]);
//     // }
//     // for(int i=0;i<=nz;i++){
//     //   printf("%i ", i);
//     //   for(int j=1; j<=nx+1; j++){
//     //     int idx = (j-1)+i*(nx+1);
//     //     printf("%0.17f ", dp[idx]);
//     //   }
//     //   printf("\n");
//     // }
//     __syncthreads();
//
//     // HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));
//     // Step 4.2: Predict Wew Velocities
//     dim3 gridPredictNewVelocity(nz-1);
//     dim3 blockPredictNewVelocity(nx);
//     predictNewVelocity<<< gridPredictNewVelocity, blockPredictNewVelocity >>>(un, ustar, wn, wstar, dp, wet);
//     cudaDeviceSynchronize();
//
//     // Step 4.3: Predict Depth-integrated flow
//     dim3 gridPredictDepthIntegratedFlow(nx);
//     dim3 blockPredictDepthIntegratedFlow(1);
//     predictDepthIntegratedFlow<<< gridPredictDepthIntegratedFlow, blockPredictDepthIntegratedFlow >>>(q, un);
//     cudaDeviceSynchronize();
//     // HANDLE_ERROR(cudaMemcpy(h_q, d_q, (nx+2)*sizeof(h_q[0]), cudaMemcpyDeviceToHost));
//     // for(int i=0;i<nx;i++){
//     //   printf("%i %f\n",i,h_q[i]);
//     // }
//     // Lateral boundary condition
//     lateralBoundaryConditionQ<<<1,1>>>(q);
//     cudaDeviceSynchronize();
//     // step 4.4: predict surface pressure field
//     predictSurfacePressureField<<<nx-1,1>>>(dp, dpstore, q);
//     cudaDeviceSynchronize();
//     // for(i=0;i<=nz;i++){
//     //   printf("%i ", i);
//     //   for( j=1; j<=nx+1; j++){
//     //     int idx = (j-1)+i*(nx+1);
//     //     printf("%f ", dp[idx]);
//     //   }
//     //   printf("\n");
//     // }
//
//     // printf("perrrrrr ----------- %f\n", perr);
//     if(perr <= d_peps){
//       nstop = nsor;
//     }
//   }
// }

__global__ void updatingNextTimeStep(double *u, double *un, double *w, double *wn){
  int idx = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  u[idx]  = un[idx];
  w[idx]  = wn[idx];
}

__global__ void lateralBoundaryCondition(double *u){
  u[blockIdx.x*d_nx+1] = 0.0;
  u[blockIdx.x*d_nx+d_nx-1] = 0.0;
  u[blockIdx.x*d_nx+d_nx] = 0.0;
}
