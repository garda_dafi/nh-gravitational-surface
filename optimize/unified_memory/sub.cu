#include "sub_kernel.cu"

void init(){
  dim3 gridWet(nz+1);
  dim3 blockWet(nx+1);
  initWet<<<gridWet,blockWet>>>(d_wet);

  dim3 gridWetFalse(nz+1);
  dim3 blockWetFalse(1);
  initWetFalse<<<gridWetFalse,blockWetFalse>>>(d_wet);

  dim3 gridBottomTopograhy(1);
  dim3 blockBottomTopograhy(nx-2);
  initBottomTopography<<<gridBottomTopograhy, blockBottomTopograhy>>>(d_depth);

  dim3 gridParamSOR(nz-1);
  dim3 blockParamSOR(nx);
  initParamSOR<<<gridParamSOR, blockParamSOR>>>(d_ct, d_cb, d_ce, d_cw, d_ctot, d_wet);

  // Synchronize init data
  cudaDeviceSynchronize();
}

void dyn(){
  int __attribute__((unused)) nsor, nstop;
  double perr;

  // seal-level forcing
  d_dp[1] = h_ad*RHO*G;
  // HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));

  // Surface pressure field
  dim3 gridSurfacePressureField(1);
  dim3 blockSurfacePressureField(nx);
  surfacePressureField<<<gridSurfacePressureField, blockSurfacePressureField>>>(d_dpstore, d_dp);

  // calculate ustar and wstar
  dim3 gridUstarAndWstar(nz-1);
  dim3 blockUstarAndWstar(nx);
  calculateUstarAndWstar<<<gridUstarAndWstar, blockUstarAndWstar>>>(d_ustar, d_u, d_wstar, d_w, d_dp, d_wet);

  // Step 3: calculate right-hand side of poisson equation
  dim3 gridCalculatePoisson(nz-1);
  dim3 blockCalculatePoisson(nx);
  calculatePoisson<<<gridCalculatePoisson, blockCalculatePoisson>>>(d_pstar,d_ustar, d_u, d_wstar, d_w);

  // HANDLE_ERROR(cudaMemcpy(h_ctot, d_ctot, nxz*sizeof(h_ctot[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(h_dp[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_ct, d_ct, nxz*sizeof(h_ct[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_cb, d_cb, nxz*sizeof(h_cb[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_ce, d_ce, nxz*sizeof(h_ce[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_cw, d_cw, nxz*sizeof(h_cw[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_pstar, d_pstar, nxz*sizeof(h_pstar[0]), cudaMemcpyDeviceToHost));
  // HANDLE_ERROR(cudaMemcpy(h_wet, d_wet, nxz*sizeof(h_wet[0]), cudaMemcpyDeviceToHost));
  // Synchronize data
  // cudaDeviceSynchronize();
  //
  // dim3 gridPredictNewPressure(1);
  // dim3 blockPredictNewPressure(1);
  // algorithmSOR<<<gridPredictNewPressure, blockPredictNewPressure>>>(
  //   d_ct    , d_cb      , d_ce , d_cw    , d_ctot,
  //   d_pstar , d_dp      , d_wet
  // );
  // cudaDeviceSynchronize();
  // for(int i=0;i<=nz;i++){
  //   printf("%i ", i);
  //   for(int j=0; j<=nx;j++){
  //     printf("%0.17f ", d_dp[j+i*(nx+1)]);
  //   }
  //   printf("\n");
  // }
  cudaDeviceSynchronize();
  int i        , j       , idx      , lBound, rBound,
      idxZplus , idxZmin , idxXplus , idxXmin;
  double h_p1, h_p2, h_term1;
  nstop = 1000;
  for(nsor = 1; nsor <= nstop; nsor++){
    perr = 0.0;
    // Step 4.1: predict new pressure
    for(i = 0; i<=(nz-2); i++){
      for(j = 0; j<=nx; j++){
        idx = j + (i+1) * (nx+1);
        lBound = i * (nx+1);
        rBound = lBound + nx;
        if((idx != lBound) && (idx != rBound)){
          idxZplus = j + ((i+1)+1) * (nx+1);
          idxZmin  = j + ((i+1)-1) * (nx+1);
          idxXplus = (j+1) + (i+1) * (nx+1);
          idxXmin  = (j-1) + (i+1) * (nx+1);
          if(d_wet[idx]){
            h_p1 = d_dp[idx];
            h_term1 = d_pstar[idx] + \
            (d_ct[idx] * d_dp[idxZmin] + d_cb[idx] * d_dp[idxZplus]) + \
            (d_cw[idx] * d_dp[idxXmin] + d_ce[idx] * d_dp[idxXplus]);
            h_p2 = (1.0 - omega) * h_p1 + omega * h_term1 / d_ctot[idx];
            d_dp[idx] = h_p2;
            perr = fmax(fabs(h_p2 - h_p1), perr);
          } // endif
        }
      }
    }
    for(i=0; i<=(nz-2); i++){
      lBound = (i+1)*(nx+1);
      rBound = lBound + nx;
      d_dp[lBound] = d_dp[lBound + 1];
      d_dp[rBound] = d_dp[rBound - 1];
    }
    if(perr <= h_peps){
      nstop = nsor;
      // printf("huhuy");
    }
  }// end nsor
  // HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));
  // HANDLE_ERROR(cudaMemcpy(d_ctot, h_ctot, nxz*sizeof(h_ctot[0]), cudaMemcpyHostToDevice));
  // HANDLE_ERROR(cudaMemcpy(d_ct, h_ct, nxz*sizeof(h_ct[0]), cudaMemcpyHostToDevice));
  // HANDLE_ERROR(cudaMemcpy(d_cb, h_cb, nxz*sizeof(h_cb[0]), cudaMemcpyHostToDevice));
  // HANDLE_ERROR(cudaMemcpy(d_ce, h_ce, nxz*sizeof(h_ce[0]), cudaMemcpyHostToDevice));
  // HANDLE_ERROR(cudaMemcpy(d_cw, h_cw, nxz*sizeof(h_cw[0]), cudaMemcpyHostToDevice));
  // HANDLE_ERROR(cudaMemcpy(d_pstar, h_pstar, nxz*sizeof(h_pstar[0]), cudaMemcpyHostToDevice));
  // Step 4.2: Predict Wew Velocities
  dim3 gridPredictNewVelocity(nz-1);
  dim3 blockPredictNewVelocity(nx);
  predictNewVelocity<<< gridPredictNewVelocity, blockPredictNewVelocity >>>(d_un, d_ustar, d_wn, d_wstar, d_dp, d_wet);


  // Step 4.3: Predict Depth-integrated flow
  dim3 gridPredictDepthIntegratedFlow(1);
  dim3 blockPredictDepthIntegratedFlow(nx);
  predictDepthIntegratedFlow<<< gridPredictDepthIntegratedFlow, blockPredictDepthIntegratedFlow >>>(d_q, d_un);

  // Lateral boundary condition
  lateralBoundaryConditionQ<<<1,1>>>(d_q);

  // step 4.4: predict surface pressure field
  predictSurfacePressureField<<<1,nx>>>(d_dp, d_dpstore, d_q);
  // HANDLE_ERROR(cudaMemcpy(h_q, d_q, (nx+1)*sizeof(h_un[0]), cudaMemcpyDeviceToHost));

  // Updating for next time step
  dim3 gridUpdatingNextTimeStep(nz-1);
  dim3 blockUpdatingNextTimeStep(nx);
  updatingNextTimeStep<<<gridUpdatingNextTimeStep, blockUpdatingNextTimeStep>>>(d_u, d_un, d_w, d_wn);


  // Lateral boundary conditions
  dim3 gridLateralBoundaryCondition(nz);
  dim3 blockLateralBoundaryCondition(1);
  lateralBoundaryCondition<<<gridLateralBoundaryCondition, blockLateralBoundaryCondition>>>(d_u);
  cudaDeviceSynchronize();
} // end dyn
