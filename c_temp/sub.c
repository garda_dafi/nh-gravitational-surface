void init(){
  int nb;

  // set all arrays to zero
  for(i=0;i<=nz+1;i++){
    for( j=0; j<=nx+1; j++){
      dp[i][j] = 0.0;
      u[i][j] = 0.0;
      un[i][j] = 0.0;
      ustar[i][j] = 0.0;
      w[i][j] = 0.0;
      wn[i][j] = 0.0;
      wstar[i][j] = 0.0;
      wet[i][j] = true;
    }
  }

  for(i=0; i<=nx+1; i++){
    q[i] = 0.0;
  }

  for(i=0; i<=nz+1; i++){
    wet[i][0] = false;
    wet[i][nx+1] = false;
  }

  // variable bottom topography
  for(i=1; i<= 51; i++){
    depth[i] = 100.0-95.0*(double)i/(double)51;
  }

  for(i=52; i<=nx; i++){
    depth[i] = 5.0+95.0*(double)(i-51)/(double)(nx-51);
  }

  // for(i=0;i<=nx;i++){
  //   printf("%i %f\n",i, depth[i]);
  // }


  // open and write file
  // FILE *h;
  // h = fopen("h.dat", "w+");
  // for(i=1; i<=nx; i++){
  //   fprintf(h, "%f\n", depth[i]);
  // }
  // fclose(h);

  // wet/dry pointer
  // for(i=1; i<=nx; i++){
  //   nb = (int)(depth[i]/dz);
  //   for( j=nb; j<=nz+1; j++){
  //     wet[j][i] = false;
  //   }
  // }
  // for(i=1; i<=nz; i++){
  //   nb = (int)(depth[i]/dz);
  //   for( j=nb+30; j<= nx/*nz+1*/; j++){
  //     // if((i+70)<j){
  //       wet[i][j] = false;
  //     // }
  //   }
  // }


  for(i=1; i<=nz; i++){
    for( j=1; j<=nx; j++){
      ct[i][j] = (double)(dx/dz);
      cb[i][j] = (double)(dx/dz);
      ce[i][j] = (double)(dz/dx);
      cw[i][j] = (double)(dz/dx);
      if(!wet[i][j-1]){
        cw[i][j] = 0.0;
      }
      if(!wet[i][j+1]){
        ce[i][j] = 0.0;
      }
      if(!wet[i+1][j]){
        cb[i][j] = 0.0;
      }
      if(!wet[i-1][j]){
        ct[i][j] = 0.0;
      }
      ctot[i][j] = cb[i][j] + ct[i][j] + ce[i][j] + cw[i][j];
    }
  }
} // end init

void dyn(){
  // local parameters
  double pressx, drdxh, pressz, drdzh;
  double dpstore[nx+1], perr, p1, p2, term1;
  int nsor, nstop;

  drdxh = 0.5/(RHO*dx);
  drdzh = 0.5/(RHO*dz);

  // sea-level forcing
  dp[0][1] = ad*RHO*G;

  // Step 1: Store surface pressure field
  for(i=0; i<=nx+1; i++){
    dpstore[i] = dp[0][i];
  }
  // for(i=0; i<=nx+1; i++){
  //   printf("%f\n", dpstore[i]);
  // }


  // Step 2: Calculate ustar and Wstar
  for(i=1; i<=nz; i++){
    for( j=1; j<=nx; j++){
      if(wet[i][j]){
        pressx = -drdxh*(dp[i][j+1] - dp[i][j]);
        if(wet[i][j+1]) {
          ustar[i][j] = u[i][j] + dt*pressx;
        }
        pressz = -drdzh*(dp[i-1][j] - dp[i][j]);
        if(wet[i-1][j]){
          wstar[i][j] = w[i][j] + dt*pressz;
        }
      }
    }
  }

  // Step 3: calculate right-hand side of poisson equation
  for(i=1; i<=nz; i++){
    for( j=1; j<=nx; j++){
      pstar[i][j] = -2.0*RHO/dt * ( \
      (ustar[i][j] - u[i][j] - ustar[i][j-1] + u[i][j-1]) * dz + \
      (wstar[i][j] - w[i][j] - wstar[i+1][j] + w[i+1][j]) * dx);
    }
  }

  for(i = 1; i<= nz; i++){
    for( j = 1; j<= nx; j++){
      if(wet[i][j]){
        p1 = dp[i][j];
        term1 = pstar[i][j] + \
        ct[i][j] * dp[i-1][j] + cb[i][j] * dp[i+1][j] + \
        cw[i][j] * dp[i][j-1] + ce[i][j] * dp[i][j+1];
        p2 = (1.0 - omega) * p1 + omega * term1 / ctot[i][j];
        dp[i][j] = p2;
      } // endif
    } // endfor
  } // endfor

  for(i=1; i<=nz; i++){
    dp[i][0] = dp[i][1];
    dp[i][nx+1] = dp[i][nx];
  }

  // Step 4: S.O.R Iteration
  nstop = 1000;
  for(nsor = 1; nsor<= nstop; nsor++){
    perr = 0.0;

    // Step 4.1: predict new pressure
    // red
    for(i = 1; i<= nz; i+=2){
      for( j = 1; j<= nx; j+=2){
        if(wet[i][j]){
          p1 = dp[i][j];
          term1 = pstar[i][j] + \
          ct[i][j] * dp[i-1][j] + cb[i][j] * dp[i+1][j] + \
          cw[i][j] * dp[i][j-1] + ce[i][j] * dp[i][j+1];
          p2 = (1.0 - omega) * p1 + omega * term1 / ctot[i][j];
          dp[i][j] = p2;
          perr = MAX(fabs(p2 - p1), perr);
        } // endif
      } // endfor
    } // endfor

    // black
    for(i = 2; i<= nz; i+=2){
      for( j = 2; j<= nx; j+=2){
        if(wet[i][j]){
          p1 = dp[i][j];
          term1 = pstar[i][j] + \
          ct[i][j] * dp[i-1][j] + cb[i][j] * dp[i+1][j] + \
          cw[i][j] * dp[i][j-1] + ce[i][j] * dp[i][j+1];
          p2 = (1.0 - omega) * p1 + omega * term1 / ctot[i][j];
          dp[i][j] = p2;
          perr = MAX(fabs(p2 - p1), perr);
        } // endif
      } // endfor
    } // endfor
    for(i = 1; i<= nz; i+=2){
      for( j = 2; j<= nx; j+=2){
        if(wet[i][j]){
          p1 = dp[i][j];
          term1 = pstar[i][j] + \
          ct[i][j] * dp[i-1][j] + cb[i][j] * dp[i+1][j] + \
          cw[i][j] * dp[i][j-1] + ce[i][j] * dp[i][j+1];
          p2 = (1.0 - omega) * p1 + omega * term1 / ctot[i][j];
          dp[i][j] = p2;
          perr = MAX(fabs(p2 - p1), perr);
        } // endif
      } // endfor
    } // endfor
    for(i = 2; i<= nz; i+=2){
      for( j = 1; j<= nx; j+=2){
        if(wet[i][j]){
          p1 = dp[i][j];
          term1 = pstar[i][j] + \
          ct[i][j] * dp[i-1][j] + cb[i][j] * dp[i+1][j] + \
          cw[i][j] * dp[i][j-1] + ce[i][j] * dp[i][j+1];
          p2 = (1.0 - omega) * p1 + omega * term1 / ctot[i][j];
          dp[i][j] = p2;
          perr = MAX(fabs(p2 - p1), perr);
        } // endif
      } // endfor
    } // endfor

    // for(i = 1; i<= nz; i++){
    //   for( j = 1; j<= nx; j++){
    //     if(wet[i][j]){
    //       dp[i][j] = temp[i][j];
    //     } // endif
    //   } // endfor
    // } // endfor

    for(i=1; i<=nz; i++){
      dp[i][0] = dp[i][1];
      dp[i][nx+1] = dp[i][nx];
    }


    // printf("----------------------- Error %f => %i\n", perr, nsor);
    // for(i=0;i<=nz+1;i++){
    //   printf("%i ",i);
    //   for(j=0;j<=nx+1;j++){
    //     printf("%i ", (int)dp[i][j]);
    //   }
    //   printf("\n");
    // }

    // Step 4.2: predict new velocities
    for(i = 1; i<=nz; i++){
      for( j=1; j<=nx; j++){
        if(wet[i][j]){
          pressx = -drdxh*(dp[i][j+1] - dp[i][j]);
          if(wet[i][j+1]){
            un[i][j] = ustar[i][j] + dt*pressx;
            pressz = -drdzh * (dp[i-1][j] - dp[i][j]);
          }
          if(wet[i-1][j]){
            wn[i][j] = wstar[i][j] + dt*pressz;
          } // endif
        }
      } // endfor
    } // endfor

    // Step 4.3: predict depth-integrated flow
    for(i=1; i<= nx; i++){
      // printf("%i\t", i);
      q[i] = 0.0;
      for( j = 1; j<=nz; j++){
        q[i] = q[i] + dz*un[j][i];
        // q[i] = un[j][i];
        // printf("%0.7e\t", un[j][i]);
      } // endfor
      // printf("\n");
    } // endfor

    // lateral boundary conditions
    q[0] = 0.0;
    q[nx] = 0.0;
    q[nx+1] = q[nx];
    // for(i=0;i<=nz+1;i++){
    //   printf("%i %0.7e\n",i, q[i]);
    // }

    // for(i=0;i<=nx;i++){
    //   printf("%i %f\n",i, q[i]);
    // }

    // step 4.4: predict surface pressure field
    for(i = 1;i<=nx; i++){
      dp[0][i] = dpstore[i] - dt*RHO*G*(q[i] - q[i-1]) / dx;
    }
    if(perr <= peps){
      printf("----------------------- %f => %i\n", perr, nsor);
      nstop = nsor;
      // printf("No. of Interaction => %d | ", nstop);
      // here++;
      // printf("on data %i\n", here);
    }
  } // endfor nsor
  // updating for next time step
  for(i=1; i<= nz; i++){
    for( j=1; j<= nx; j++){
      u[i][j] = un[i][j];
      w[i][j] = wn[i][j];
    }
  }
  // for(i=0;i<=nz+1;i++){
  //   printf("%i ", i);
  //   for( j=0; j<=nx+1; j++){
  //     printf("%0.7e ", w[i][j]);
  //   }
  //   printf("\n");
  // }

  // lateral boundary condition
  for(i=1; i<= nz+1; i++){
    u[i][0] = 0.0;
    u[i][nx] = 0;
    u[i][nx+1] = 0;
  }
  // for(i=0;i<=nx+1;i++){
    // printf("%i %0.7e\n",i, dp[0][i]);
  // }
  // for(i=0;i<=nz+1;i++){
  //   printf("%i ", i);
  //   for( j=99; j<=nx+1; j++){
  //     printf("%0.7e ", dp[i][j]);
  //   }
  //   printf("\n");
  // }
} // end dyn
