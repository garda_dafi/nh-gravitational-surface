#include <stdio.h>
#include <stdlib.h>
#define nEta1  10100
#define nEta   101
#define nDp1   515100
#define nDp    5151
#define nTime  100

int main(){
  float *eta1,
        *eta,
        *dp1,
        *dp;
  eta1  = (float*)malloc(nEta1*sizeof(float));
  eta   = (float*)malloc(nEta*sizeof(float));
  dp1   = (float*)malloc(nDp1*sizeof(float));
  dp    = (float*)malloc(nDp*sizeof(float));
  int count = 0,
      itop  = 0,
      ibot  = 0,
      t     = 0,
      x     = 0,
      i     = 0,
      j     = 0;
  FILE *etaDat = fopen("eta.dat", "r");
  FILE *dpDat = fopen("dp.dat", "r");

  if(!etaDat){
    printf("Error: eta.dat tidak dapat dibaca!\n");
  }else{
    printf("File eta.dat berhasil dibuka\n");
  }

  if(!dpDat){
    printf("Error: dp.dat tidak dapat dibaca!\n");
  }else{
    printf("File dp.dat berhasil dibuka\n");
  }

  count = 0;
  while(!feof(etaDat)){
    fscanf(etaDat, "%f", &eta1[count]);
    count++;
  }
  count = 0;
  while(!feof(dpDat)){
    fscanf(dpDat, "%f", &dp1[count]);
    count++;
  }

  // for(i=0; i<nEta; i++){
  //   printf("Eta => %f\n", eta1[i]);
  // }
  // for(i=0; i<nDp; i++){
  //   printf("dp => %f\n", dp[i]);
  // }
  FILE *gnuplot = popen("gnuplot", "w");
  if(gnuplot == NULL){
    printf("GNU Plot tidak dapat dibuka\n");
  }else{
    // fprintf(gnuplot, "reset\n");
    // fprintf(gnuplot, "set xrange[0:500]\n");
    // fprintf(gnuplot, "set yrange[-40:10]\n");
    // animation loop
    for(t = 1; t<= nTime; t++){
      itop = (t-1)*51+1;
      ibot = itop+50;
      // fprintf(gnuplot, "plot '-' with lines\n");
      for(i = itop; i<= ibot; i++){
        for(j = 0; j<= 101; j++){
          dp[i] = dp1[i];
        }
      }
      x = 0;
      for(i = t*nTime; i< nTime+t*nTime; i++){
        eta[i] = 5*eta1[i];
        // fprintf(gnuplot, "%i %f\n", x, 5*eta1[i]);
        x += 5;
      }
      // fprintf(gnuplot, "end\n");
      // fprintf(gnuplot, "pause 0.1\n");
    }
  }
  pclose(gnuplot);

  free(eta1);
  free(eta);
  free(dp1);
  free(dp);
  fclose(etaDat);
  fclose(dpDat);
  return 0;

}
