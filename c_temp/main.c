#include "param.c"
#include "sub.c"
#include "../cpuSecond.h"

int main(){
  int n, ntot, nout;
  double wl, ps;

  init_param();
  wl = G*period*period/(2.*PI);
  printf("Depp-water wavelength (m) is %f\n", wl);
  ps = wl/period;
  printf("Deep-water phase speed (m/s) is %f\n", ps);

  init();

  // runtime parameters
  ntot = (int)(100./dt);
  printf("ntot %d\n", ntot);
  double time = 0.0;

  // output parameters
  nout = (int)(1/dt);

  FILE *dp_file, *u_file, *w_file, *eta_file;
  dp_file = fopen("dp.dat", "w+");
  u_file = fopen("u.dat", "w+");
  w_file = fopen("w.dat", "w+");
  eta_file = fopen("eta.dat", "w+");

  // // simulation loop
  double iStart = cpuSecond();
  // ntot = 1;
  for(n=1; n<=ntot; n++){
    time += dt;

    // variation of forcing
    printf("ad %i\t=> %0.8e  |\n",n, (double)sin(2.*PI*time/period));
    ad = amplitude*sin(2.*PI*time/period);

    // prognostic equations
    dyn();
  //
    if((n%nout) == 0){
    // if(n == 1){
      printf("Data output at time = %f\n", time/(24.*3600.));
      // printf("%i\n", n);
      for(i = 1; i<= nz; i++){
        // printf("%i ", i);
        for(j=1; j<=nx; j++){
          fprintf(dp_file, "%f\n", dp[i][j]/(RHO*G));
          // printf("%f ", dp[i][j]/(RHO*G));
          fprintf(u_file, "%f\n", u[i][j]);
          fprintf(w_file, "%f\n", w[i][j]);
        }
      }
      for(i=1; i<=nx; i++){
        fprintf(eta_file, "%f\n", dp[0][i]/(RHO*G));
        // printf("%0.17f\n", dp[0][i]/(RHO*G));
      }

    }
    // if(n == ntot){
    //   // for(i=1; i<=nx; i++){
    //   //   printf("%i %f\n",i, dp[0][i]/(RHO*G));
    //   //   // printf("%0.17f\n", dp[0][i]/(RHO*G));
    //   // }
    //   for(i=1;i<=nz;i++){
    //     printf("%i ", i);
    //     for( j=1; j<=nx; j++){
    //       printf("%f ", dp[i][j]/(RHO*G));
    //     }
    //     printf("\n");
    //   }
    // }
  }
  double iElaps = cpuSecond() - iStart;
  printf("Time elapsed %0.17f s\n", iElaps);

  fclose(dp_file);
  fclose(u_file);
  fclose(w_file);
  fclose(eta_file);
  free(dp);
  free(u);
  free(un);
  free(ustar);
  free(w);
  free(wn);
  free(wstar);
  free(wet);
  free(pstar);
  free(q);
  free(ct);
  free(cb);
  free(cw);
  free(ce);
  free(ctot);
  return 0;
}
