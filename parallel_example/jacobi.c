int Diffusion_Jacobi(int N, double dx, double dt, double **A, double **q, double abstol){
  int i,j,k;
  int maxit = 100000;
  double sum;
  double ** Aold = CreateMatrix(N,N);
  double D = dt/(dx*dx);
  for(i=1; i<N-1; i++)
    for(j=1;j<N-1;j++)
      Aold[i][j] = 1.0;
  /* Boundary Conditions -- all zeros */
  for(i=0;i<N;i++){
    A[0][i] = 0.0;
    A[N-1][i] = 0.0;
    A[i][0] = 0.0;
    A[i][N-1] = 0.0;
  }
  for(k=0; k<maxit; k++){
    for(i = 1; i<N-1; i++){
      for(j=1; j<N-1; j++){
        A[i][j] = dt*q[i][j] + Aold[i][j] + D*(Aold[i+1][j] + Aold[i][j+1] - 4.0*Aold[i][j] + Aold[i-1][j] + Aold[i][j-1]);
      }
    }
    sum = 0.0;
    for(i=0;i<N;i++){
      for(j=0;j<N;j++){
        sum += (Aold[i][j]-A[i][j])*(Aold[i][j]-A[i][j]);
        Aold[i][j] = A[i][j];
      }
    }
    if(sqrt(sum)<abstol){
      DestroyMatrix(Aold,N,N);
      return k;
    }
  }
  cerr << "Jacobi: Maximum Number of Interations Reached \n";
  DestroyMatrix(Aold,N,N);
  return maxit;
}
