#include "sub_kernel.cu"

void init(){
  dim3 gridWet(nz+1);
  dim3 blockWet(nx+2);
  initWet<<<gridWet,blockWet>>>(d_wet);
  cudaDeviceSynchronize();

  dim3 gridWetFalse(nz+2);
  dim3 blockWetFalse(1);
  initWetFalse<<<gridWetFalse,blockWetFalse>>>(d_wet);
  cudaDeviceSynchronize();

  dim3 gridBottomTopograhy(1);
  dim3 blockBottomTopograhy(nx-2);
  initBottomTopography<<<gridBottomTopograhy, blockBottomTopograhy>>>(d_depth);
  cudaDeviceSynchronize();


  dim3 gridWetAndDry(1);
  dim3 blockWetAndDry(1);
  initWetAndDryPointer<<<gridWetAndDry, blockWetAndDry>>>(d_depth, d_wet);
  // Synchronize init data
  cudaDeviceSynchronize();
  dim3 gridParamSOR(nz-1);
  dim3 blockParamSOR(nx);
  initParamSOR<<<gridParamSOR, blockParamSOR>>>(d_ct, d_cb, d_ce, d_cw, d_ctot, d_wet);
  cudaDeviceSynchronize();
  // HANDLE_ERROR(cudaMemcpy(h_wet, d_wet, nxz*sizeof(h_wet[0]), cudaMemcpyDeviceToHost));
  // for(int i=0;i<=nz;i++){
  //   printf("%i ", i);
  //   for(int j=1; j<=nx+1; j++){
  //     int idx = (j-1)+i*(nx+1);
  //     printf("%i", h_wet[idx]);
  //   }
  //   printf("\n");
  // }
}

void dyn(){
  int __attribute__((unused)) nsor, nstop, perr;

  HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(d_dp[0]), cudaMemcpyDeviceToHost));
  // seal-level forcing
  h_dp[1] = h_ad*RHO*G;

  // printf("h_dp1 = %f\n", h_dp[1]);
  HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));

  // Surface pressure field
  // dim3 gridSurfacePressureField(nx+1);
  // dim3 blockSurfacePressureField(1);
  surfacePressureField<<<nx+1, 1>>>(d_dpstore, d_dp);
  HANDLE_ERROR(cudaMemcpy(h_dpstore, d_dpstore, (nx+2)*sizeof(d_dp[0]), cudaMemcpyDeviceToHost));
  // for(int i=0; i<nx+1; i++){
  //   printf("%0.7e\n", h_dpstore[i]);
  // }
  // cudaDeviceSynchronize();

  // calculate ustar and wstar
  // dim3 gridUstarAndWstar(nz-1);
  // dim3 blockUstarAndWstar(nx);
  calculateUstarAndWstar<<<nz-1, nx>>>(d_ustar, d_u, d_wstar, d_w, d_dp, d_wet);
  // cudaDeviceSynchronize();

  // Step 3: calculate right-hand side of poisson equation
  // dim3 gridCalculatePoisson(nz-1);
  // dim3 blockCalculatePoisson(nx);
  calculatePoisson<<<nz-1, nx>>>(d_pstar,d_ustar, d_u, d_wstar, d_w);

  // Synchronize data
  cudaDeviceSynchronize();

  // Step 4: S.O.R Iteration
  HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(h_dp[0]), cudaMemcpyDeviceToHost));
  HANDLE_ERROR(cudaMemcpy(h_pstar, d_pstar, nxz*sizeof(h_pstar[0]), cudaMemcpyDeviceToHost));
    int i        , j       , idx      , lBound, rBound,
        idxZplus , idxZmin , idxXplus , idxXmin;
    double h_p1, h_p2, h_term1;
    nstop = 1000;
    for(nsor = 1; nsor <= nstop; nsor++){
      HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(h_dp[0]), cudaMemcpyDeviceToHost));
      double perr = 0.0;
      // Step 4.1: predict new pressure
      for(i = 0; i<nz-1; i++){
        for(j = 0; j<nx; j++){
          idx    = j + (i+1)*(nx+1);
          lBound = (i+1) * (nx+1);
          rBound = lBound + nx;
          if((idx != lBound) && (idx != rBound)){
            idxZplus = j + ((i+1)+1) * (nx+1);
            idxZmin  = j + ((i+1)-1) * (nx+1);
            idxXplus = (j+1) + (i+1) * (nx+1);
            idxXmin  = (j-1) + (i+1) * (nx+1);
            if(h_wet[idx]){
              h_p1 = h_dp[idx];
              h_term1 = h_pstar[idx] + \
              (h_ct[idx] * h_dp[idxZmin] + h_cb[idx] * h_dp[idxZplus]) + \
              (h_cw[idx] * h_dp[idxXmin] + h_ce[idx] * h_dp[idxXplus]);
              h_p2 = (1.0 - omega) * h_p1 + omega * h_term1 / h_ctot[idx];
              h_dp[idx] = h_p2;
              perr = fmax(fabs(h_p2 - h_p1), perr);
            } // endif
          }
        }
      }

      for(i=0; i<nz-1; i++){
        lBound = (i+1)*(nx+1);
        rBound = lBound + nx;
        h_dp[lBound] = h_dp[lBound + 1];
        h_dp[rBound] = h_dp[rBound - 1];
      }

      HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));
      // Step 4.2: Predict Wew Velocities
      // dim3 gridPredictNewVelocity(nz-1);
      // dim3 blockPredictNewVelocity(nx);
      predictNewVelocity<<< nz-1, nx >>>(d_un, d_ustar, d_wn, d_wstar, d_dp, d_wet);
      cudaDeviceSynchronize();

      // Step 4.3: Predict Depth-integrated flow
      // dim3 gridPredictDepthIntegratedFlow(nx);
      // dim3 blockPredictDepthIntegratedFlow(1);
      predictDepthIntegratedFlow<<< nx, 1 >>>(d_q, d_un);
      cudaDeviceSynchronize();

      // Lateral boundary condition
      lateralBoundaryConditionQ<<< 1, 1 >>>(d_q);
      cudaDeviceSynchronize();

      // step 4.4: predict surface pressure field
      predictSurfacePressureField<<< nx-1, 1 >>>(d_dp, d_dpstore, d_q);
      cudaDeviceSynchronize();

      // printf("perrrrrr ----------- %f\n", perr);
      if(perr <= h_peps){
        printf("---------------------------------------------------------Nstop = %i\n",nsor);
        nstop = nsor;
      }
    }// end nsor

  // Updating for next time step
  dim3 gridUpdatingNextTimeStep(nz-1);
  dim3 blockUpdatingNextTimeStep(nx);
  updatingNextTimeStep<<<gridUpdatingNextTimeStep, blockUpdatingNextTimeStep>>>(d_u, d_un, d_w, d_wn);
  // cudaDeviceSynchronize();

  // Lateral boundary conditions
  dim3 gridLateralBoundaryCondition(nz+1);
  dim3 blockLateralBoundaryCondition(1);
  lateralBoundaryCondition<<<gridLateralBoundaryCondition, blockLateralBoundaryCondition>>>(d_u);
  cudaDeviceSynchronize();

} // end dyn
