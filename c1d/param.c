#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

#define G 9.81 // acceleration due to gravity
#define RHO 1028.0 // reference density
#define PI 3.1416 // pi

#define nx 102 // horizontal dimension
#define nz 52 // vertical dimension

// grid parameters
double dt = 0.05, dx = 5.0, dz = 2.0, ad;
int i,j;
// forcing parameters
double amplitude = 1.0;
double period    = 8.0;

// coefficients for SOR
double    omega = 1.4;
double    peps = 1.e-2;
int here = 0;
int*     wet;
double*   dp;
double*   u;
double*   un;
double*   w;
double*   wn;
double*   ustar;
double*   wstar;
double*   pstar;
double*   ct;
double*   cb;
double*   ce;
double*   cw;
double*   ctot;
double*   depth;
double*   q;

int nxz = (nx+2)*(nz+2);
void init_param(){

  dp    = (double *)malloc((nxz)*sizeof(double)); // dynamic pressure
  u     = (double *)malloc((nxz)*sizeof(double));
  un    = (double *)malloc((nxz)*sizeof(double)); // zonal speed
  ustar = (double *)malloc((nxz)*sizeof(double));
  w     = (double *)malloc((nxz)*sizeof(double));
  wn    = (double *)malloc((nxz)*sizeof(double)); // vertical speed
  wstar = (double *)malloc((nxz)*sizeof(double));
  wet   = (int   *)malloc((nxz)*sizeof(int)); // wet/dry pointer
  q     = (double *)malloc((nx)*sizeof(double)); // depth-integrated flow
  depth = (double *)malloc((nx)*sizeof(double));

  // matrices and parameters for S.O.R iteration
  ct    = (double *)malloc((nxz)*sizeof(double));
  cb    = (double *)malloc((nxz)*sizeof(double));
  ce    = (double *)malloc((nxz)*sizeof(double));
  cw    = (double *)malloc((nxz)*sizeof(double));
  ctot  = (double *)malloc((nxz)*sizeof(double));

  pstar = (double *)malloc((nxz)*sizeof(double));
}
