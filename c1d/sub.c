void init(){
  int nb,idx,lBound,rBound;
  int idxXplus, idxXmin, idxZplus, idxZmin;
  // inside calculation
  // nz - 2
  // idx = j + (i+1) *nx;
  // set all arrays to zero
  for(i=0;i<=nz;i++){
    for( j=1; j<=nx+1; j++){
      idx = (j-1)+i*(nx+1);
      dp[idx] = 0.0;
      u[idx] = 0.0;
      un[idx] = 0.0;
      ustar[idx] = 0.0;
      w[idx] = 0.0;
      wn[idx] = 0.0;
      wstar[idx] = 0.0;
      wet[idx] = 1;
    }
  }

  // for(i=0;i<nz;i++){
  //   for( j=0; j<nx; j++){
  //     idx = j+i*nx;
  //     printf("%i %f\n",idx, dp[idx]);
  //   }
  // }

  for(i=0; i<nx; i++){
    q[i] = 0.0;
  }

  for(i=0; i<nz+2; i++){
    lBound = i*(nx+1);
    rBound = lBound+nx;
    wet[lBound] = false;
    wet[rBound] = false;
  }


  // variable bottom topography
  for(i=1; i<= 51; i++){
    depth[i] = 100.0-95.0*(double)i/(double)51;
  }

  for(i=52; i<=nx; i++){
    depth[i] = 5.0+95.0*(double)(i-51)/(double)(nx-52);
  }
  // for(i=0;i<nx;i++){
  //   printf("%i %f\n",i, depth[i]);
  // }


  // open and write file
  // FILE *h;
  // h = fopen("h.dat", "w+");
  // for(i=1; i<=nx; i++){
  //   fprintf(h, "%f\n", depth[i]);
  // }
  // fclose(h);

  // wet/dry pointer - real
  // for(i=0; i<=nx; i++){
  //   nb = (int)(depth[i]/dz);
  //   for( j=nb; j<=nz+1; j++){
  //     wet[j][i] = false;
  //   }
  // }
  // for(i=0; i<nz-1; i++){
  //   nb = (int)(depth[i+1]/dz);
  //   for( j=(nb+30); j< nx/*nz+1*/; j++){
  //     // if((i+70)<j){
  //     int idx = j + (i+1)*(nx+1);
  //       wet[idx] = false;
  //     // }
  //   }
  // }

    // for(i=0;i<nz;i++){
    //   printf("%i ", i);
    //   for( j=0; j<nx+1; j++){
    //     int idx = j+i*(nx+1);
    //     printf("%i", wet[idx]);
    //   }
    //   printf("\n");
    // }


  for(i=0; i<nz-1; i++){
    for(j=0; j<nx; j++){
      idx      = j + (i+1)*(nx+1);
      idxZplus = j + ((i+1)+1)*(nx+1);
      idxZmin  = j + ((i+1)-1)*(nx+1);
      idxXplus = (j+1) + (i+1)*(nx+1);
      idxXmin  = (j-1) + (i+1)*(nx+1);
      lBound   = (i+1)*(nx+1);
      rBound   = lBound+nx;

      if((idx != lBound) && (idx != rBound)){
        ct[idx] = (double)(dx/dz);
        cb[idx] = (double)(dx/dz);
        ce[idx] = (double)(dz/dx);
        cw[idx] = (double)(dz/dx);
        if(!wet[idxXmin]){
          cw[idx] = 0.0;
        }
        if(!wet[idxXplus]){
          ce[idx] = 0.0;
        }
        if(!wet[idxZplus]){
          cb[idx] = 0.0;
        }
        if(!wet[idxZmin]){
          ct[idx] = 0.0;
        }
        ctot[idx] = cb[idx] + ct[idx] + ce[idx] + cw[idx];
      }
    }
  }
} // end init

void dyn(){
  // local parameters
  double pressx, drdxh, pressz, drdzh;
  double dpstore[nx+1], perr, p1, p2, term1;
  int nb,idx,lBound,rBound;
  int idxXplus, idxXmin, idxZplus, idxZmin;
  int nsor, nstop;

  drdxh = 0.5/(RHO*dx);
  drdzh = 0.5/(RHO*dz);

  // sea-level forcing
  dp[1] = ad*RHO*G;

  // Step 1: Store surface pressure field
  for(i=0; i<nx+1; i++){
    dpstore[i] = dp[i];
  }
  // for(i=0; i<nx+1; i++){
  //   printf("%f\n", dpstore[i]);
  // }


  // Step 2: Calculate ustar and Wstar
  for(i=0; i<nz-1; i++){
    for( j=0; j<nx; j++){
      idx      = j + (i+1)*(nx+1);
      idxZplus = j + ((i+1)+1)*(nx+1);
      idxZmin  = j + ((i+1)-1)*(nx+1);
      idxXplus = (j+1) + (i+1)*(nx+1);
      idxXmin  = (j-1) + (i+1)*(nx+1);
      lBound   = (i+1)*(nx+1);
      rBound   = lBound+nx;
      if((idx != lBound) && (idx != rBound)){
        if(wet[idx]){
          pressx = -drdxh*(dp[idxXplus] - dp[idx]);
          if(wet[idxXplus]) {
            ustar[idx] = u[idx] + dt*pressx;
          }
          pressz = -drdzh*(dp[idxZmin] - dp[idx]);
          if(wet[idxZmin]){
            wstar[idx] = w[idx] + dt*pressz;
          }
        }
      }
    }
  }

  // Step 3: calculate right-hand side of poisson equation
  for(i=0; i<nz-1; i++){
    for( j=0; j<nx; j++){
      idx      = j + (i+1)*(nx+1);
      idxZplus = j + ((i+1)+1)*(nx+1);
      idxZmin  = j + ((i+1)-1)*(nx+1);
      idxXplus = (j+1) + (i+1)*(nx+1);
      idxXmin  = (j-1) + (i+1)*(nx+1);
      lBound   = (i+1)*(nx+1);
      rBound   = lBound+nx;
      if((idx != lBound) && (idx != rBound)){
        pstar[idx] = -2.0*RHO/dt * ( \
          (ustar[idx] - u[idx] - ustar[idxXmin] + u[idxXmin]) * dz + \
          (wstar[idx] - w[idx] - wstar[idxZplus] + w[idxZplus]) * dx);
      }
    }
  }


  // Step 4: S.O.R Iteration
  nstop = 1;
  for(nsor = 1; nsor<= nstop; nsor++){
    perr = 0.0;

    // Step 4.1: predict new pressure
    for(i = 0; i<nz-1; i++){
    // for(i = 0; i<1; i++){
      for( j = 0; j<nx; j++){
        idx      = j + (i+1)*(nx+1);
        idxZplus = j + ((i+1)+1)*(nx+1);
        idxZmin  = j + ((i+1)-1)*(nx+1);
        idxXplus = (j+1) + (i+1)*(nx+1);
        idxXmin  = (j-1) + (i+1)*(nx+1);
        lBound   = (i+1)*(nx+1);
        rBound   = lBound+nx;
        if((idx != lBound) && (idx != rBound)){
          if(wet[idx]){
            p1 = dp[idx];
            term1 = pstar[idx] + \
            ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus] + \
            cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus];
            p2 = (1.0 - omega) * p1 + omega * term1 / ctot[idx];
            dp[idx] = p2;
            perr = MAX(fabs(p2 - p1), perr);
          } // endif
        }
      } // endfor
    } // endfor


    for(i=0;i<=nz;i++){
      printf("%i ", i);
      for(j=1; j<=nx+1; j++){
        int idx = (j-1)+i*(nx+1);
        printf("%0.7e ", dp[idx]);
      }
      printf("\n");
    }
    // for(i = 1; i<nz-1; i++){
    //   for( j = 1; j<nx; j+=2){
    //     idx      = j + (i+1)*(nx+1);
    //     idxZplus = j + ((i+1)+1)*(nx+1);
    //     idxZmin  = j + ((i+1)-1)*(nx+1);
    //     idxXplus = (j+1) + (i+1)*(nx+1);
    //     idxXmin  = (j-1) + (i+1)*(nx+1);
    //     lBound   = (i+1)*(nx+1);
    //     rBound   = lBound+nx;
    //     if((idx != lBound) && (idx != rBound)){
    //       if(wet[idx]){
    //         p1 = dp[idx];
    //         term1 = pstar[idx] + \
    //         ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus] + \
    //         cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus];
    //         p2 = (1.0 - omega) * p1 + omega * term1 / ctot[idx];
    //         dp[idx] = p2;
    //         perr = MAX(fabs(p2 - p1), perr);
    //       } // endif
    //     }
    //   } // endfor
    // } // endfor
    // for(i = 1; i<nz-1; i++){
    //   for( j = 0; j<nx; j+=2){
    //     idx      = j + (i+1)*(nx+1);
    //     idxZplus = j + ((i+1)+1)*(nx+1);
    //     idxZmin  = j + ((i+1)-1)*(nx+1);
    //     idxXplus = (j+1) + (i+1)*(nx+1);
    //     idxXmin  = (j-1) + (i+1)*(nx+1);
    //     lBound   = (i+1)*(nx+1);
    //     rBound   = lBound+nx;
    //     if((idx != lBound) && (idx != rBound)){
    //       if(wet[idx]){
    //         p1 = dp[idx];
    //         term1 = pstar[idx] + \
    //         ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus] + \
    //         cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus];
    //         p2 = (1.0 - omega) * p1 + omega * term1 / ctot[idx];
    //         dp[idx] = p2;
    //         perr = MAX(fabs(p2 - p1), perr);
    //       } // endif
    //     }
    //   } // endfor
    // } // endfor
    // printf("%f\n", perr);
    for(i=0; i<nz-1; i++){
      lBound   = (i+1)*(nx+1);
      rBound   = lBound+nx;
      dp[lBound] = dp[lBound+1];
      dp[rBound] = dp[rBound-1];
      // printf("%i\t %i\n", lBound+1,rBound-1);
    }
      // for(i=1; i<=nx; i++){
      //   // fprintf(eta_file, "%f\n", dp[0][i]/(RHO*G));
      //   printf("%0.17f\n", dp[0][i]/(RHO*G));
      // }
      for(i = 0; i<nz-1; i++){
        for( j = 0; j<nx; j++){
          idx      = j + (i+1)*(nx+1);
          idxZplus = j + ((i+1)+1)*(nx+1);
          idxZmin  = j + ((i+1)-1)*(nx+1);
          idxXplus = (j+1) + (i+1)*(nx+1);
          idxXmin  = (j-1) + (i+1)*(nx+1);
          lBound   = (i+1)*(nx+1);
          rBound   = lBound+nx;
          if((idx != lBound) && (idx != rBound)){
            if(wet[idx]){
              pressx = -drdxh*(dp[idxXplus] - dp[idx]);
              pressz = -drdzh * (dp[idxZmin] - dp[idx]);
              if(wet[idxXplus]){
                un[idx] = ustar[idx] + dt*pressx;
              }
              if(wet[idxZmin]){
                wn[idx] = wstar[idx] + dt*pressz;
                // printf("%i %0.7e\n",idx, pressz);
              } // endif
            }
          }
        } // endfor
      } // endfor

      // Step 4.3: predict depth-integrated flow
      for(i=0; i<nx; i++){
        q[i] = 0.0;
        // printf("%i\t", i);
        for(j = 0; j<nz-1; j++){
          idx  = i + (j+1)*(nx+1);
          q[i] = q[i] + dz*un[idx];
          // printf("%0.7e\t", un[idx]);
          // q[i] = un[idx];
        } // endfor
        // printf("\n");
      } // endfor
      // for (i = 0; i<nx;i++) {
      //   printf("%i %f \n",i, q[i]);
      // }
      //
      // lateral boundary conditions
      q[0] = 0.0;
      q[nx-1] = 0.0;
      q[nx] = q[nx-1];
      // for(i=0;i<=nz;i++){
      for(j = 0;j<nx-1; j++){
        dp[j+1] = dpstore[j+1] - dt*RHO*G*(q[j+1] - q[j]) / dx;
      }
      // for(i=0;i<nx;i++){
      //   printf("%i %f\n",i, q[i]);
      // }
      // step 4.4: predict surface pressure field

    if(perr <= peps){
      // printf("%f => %i\n", perr, nsor);
      nstop = nsor;
      // printf("No. of Interaction => %d | ", nstop);
      // here++;
      // printf("on data %i\n", here);
    }
  } // endfor nsor
  // Step 4.2: predict new velocities
  // updating for next time step
  for(i=0; i<nz-1; i++){
    for( j=0; j<nx; j++){
      int idx      = j + (i+1)*(nx+1);
      int idxZplus = j + ((i+1)+1)*(nx+1);
      int idxZmin  = j + ((i+1)-1)*(nx+1);
      int idxXplus = (j+1) + (i+1)*(nx+1);
      int idxXmin  = (j-1) + (i+1)*(nx+1);
      u[idx] = un[idx];
      w[idx] = wn[idx];
    }
  }

  // // lateral boundary condition
  for(i=0; i<= nz; i++){
    u[i*nx+1] = 0.0;
    u[i*nx+nx-1] = 0;
    u[i*nx+nx] = 0;
  }
  // for(i=0;i<=nx;i++){
  //   printf("%i %0.7e\n",i, dp[i]);
  // }

} // end dyn
