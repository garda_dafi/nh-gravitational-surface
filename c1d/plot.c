#include <stdio.h>
#include <stdlib.h>
#define nEta1  10100
#define nEta   101
#define nDp1   525100
#define nDp    5151
#define nTime  100

int main(){
  float *eta1,
        *eta,
        *dp1;
        // *dp;
  float dp[101][51];
  eta1  = (float*)malloc(nEta1*sizeof(float));
  eta   = (float*)malloc(nEta*sizeof(float));
  dp1   = (float*)malloc(nDp1*sizeof(float));
  // dp    = (float*)malloc(nDp*sizeof(float));
  int count = 0,
      iPlot = 0,
      itop  = 0,
      ibot  = 0,
      t     = 0,
      x     = 0,
      i     = 0,
      j     = 0;
  FILE *etaDat = fopen("eta.dat", "r");
  FILE *dpDat = fopen("dp.dat", "r");

  if(!etaDat){
    printf("Error: eta.dat tidak dapat dibaca!\n");
  }else{
    printf("File eta.dat berhasil dibuka\n");
  }

  if(!dpDat){
    printf("Error: dp.dat tidak dapat dibaca!\n");
  }else{
    printf("File dp.dat berhasil dibuka\n");
  }

  count = 0;
  while(!feof(etaDat)){
    fscanf(etaDat, "%f", &eta1[count]);
    count++;
  }
  count = 0;
  while(!feof(dpDat)){
    fscanf(dpDat, "%f", &dp1[count]);
    count++;
  }

  // for(i=0; i<nEta; i++){
  //   printf("Eta => %f\n", eta1[i]);
  // }
  // for(i=0; i<nDp; i++){
  //   printf("dp => %f\n", dp[i]);
  // }
  FILE *gnuplot = popen("gnuplot", "w");
  if(gnuplot == NULL){
    printf("GNU Plot tidak dapat dibuka\n");
  }else{
    fprintf(gnuplot, "reset\n");
    fprintf(gnuplot, "set xrange[0:500]\n");
    fprintf(gnuplot, "set yrange[-40:10]\n");
    fprintf(gnuplot, "set ylabel 'z' rotate by 0\n");
    fprintf(gnuplot, "set xlabel 'x'\n");
    // fprintf(gnuplot, "set terminal gif color enhanced\n");
    // animation loop
    for(t = 0; t< 100; t++){
      fprintf(gnuplot, "set title 'Time = %i'\n",t);
      // mengisi nilai dp
      itop = (t)*51+1;
      ibot = itop+50;
      // printf("itop %i ibot %i\n",itop,ibot);
      for(i=0;i<=100;i++){
        for(j=0;j<=50;j++){
          dp[i][j] = dp1[i+j*101+t*5151];
        }
      }
      // test dp value
      // printf("%f %f %f %f\n", dp[0][0], dp[0][1], dp[0][2], dp[0][3]);
      // printf("%f %f %f %f\n", dp[1][0], dp[1][1], dp[1][2], dp[1][3]);
      // printf("%f %f %f %f\n", dp[2][0], dp[2][1], dp[2][2], dp[2][3]);

      // mengisi nilai eta
      for(i=0;i<=50;i++){
        eta[i] = eta1[i+t*101];
      }
      // test eta value
      // printf("eta => %f\n", eta[0]);
      // printf("eta => %f\n", eta[1]);
      // printf("eta => %f\n", eta[2]);
      // printf("eta => %f\n", eta[3]);

      // plot data
      // fprintf(gnuplot, "plot '-' with lines\n");
      fprintf(gnuplot, "plot '-' with lines lw 2 lc rgb '#a8c6f7' notitle ");
      for(i=0;i<=19;i++){
        fprintf(gnuplot, ",'-' with lines lw 2 lc rgb '#a8c6f7' notitle ");
      }
      // fprintf(gnuplot, ",0.4*x-155 w filledcurve above lt 1 lc rgb \"#c68943\" t ''");
      fprintf(gnuplot, "\n");
      x = 0;
      // for(i=0;i<=100;i++){
      //   // printf("%i %f\n",x, .5*eta[i]);
      //   fprintf(gnuplot, "%i %f\n", x, eta[i]);
      //   x+=5;
      // }
      // fprintf(gnuplot, "end\n");

      for(iPlot=0;iPlot<=20;iPlot++){
        x = 0;
          for(i=0;i<=100;i++){
            // if(t==21){
            //   printf("%i %f\n", x, dp[i][iPlot]+1-iPlot*2);
            // }
            fprintf(gnuplot, "%i %0.10f\n", x, 5*dp[i][iPlot]-iPlot*2);
            x+=5;
          }
          fprintf(gnuplot, "end\n");
      }
      // fprintf(gnuplot, "set output 'gif/output_%i.gif'\n", t);
      fprintf(gnuplot, "pause 0.01\n");
    }
  }
  pclose(gnuplot);

  free(eta1);
  free(eta);
  free(dp1);
  // free(dp);
  fclose(etaDat);
  fclose(dpDat);
  return 0;
  // 100&50&78.390494626947&165.922 \\
  // 200&100&251.504551833961&450.023 \\
  // 400&200&718.674928037915&1379.876 \\
  // 800&400&2129.071844185004&4700.861 \\

}
