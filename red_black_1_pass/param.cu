#include <cuda.h>
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>
static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

#define G 9.81 // acceleration due to gravity
#define RHO 1028.0 // reference density
#define PI 3.1416 // pi

#define nx 102 // horizontal dimension
#define nz 52 // vertical dimension

#define dt 0.05
#define dx 5.0
#define dz 2.0

__device__ double d_G     = 9.81;
__device__ double d_RHO   = 1028.0;
__device__ double d_PI    = 3.1416;
__device__ int    d_nx    = 102;
__device__ int    d_nz    = 52;
__device__ double d_dt    = 0.05;
__device__ double d_dx    = 5.0;
__device__ double d_dz    = 2.0;

__device__ double
                  d_pressx, d_pressz,
                  d_drdxh,  d_drdzh;
double h_perr;

// one dimension data
int nxz = (nx+2)*(nz+2);
// grid parameters
double h_ad;
__device__ double d_ad;

// forcing parameters
double amplitude = 1.0;
double period    = 8.0;
__device__ double d_amplitude = 1.0;
__device__ double d_period    = 8.0;

// coefficients for SOR
double    omega = 1.4;
double    h_peps = 1.e-2;
__device__ double    d_omega = 1.4;
__device__ double    d_peps = 1.e-2;
int       here = 0;

// variable host and device
int       *h_wet,     *d_wet;
double    *h_dp,      *d_dp;
double    *h_u,       *d_u;
double    *h_un,      *d_un;
double    *h_ustar,   *d_ustar;
double    *h_w,       *d_w;
double    *h_wn,      *d_wn;
double    *h_wstar,   *d_wstar;
double    *h_pstar,   *d_pstar;
double    *h_ct,      *d_ct;
double    *h_cb,      *d_cb;
double    *h_ce,      *d_ce;
double    *h_cw,      *d_cw;
double    *h_ctot,    *d_ctot;
double    *h_q,       *d_q;
double    *h_dpstore, *d_dpstore;
double    *h_depth,   *d_depth;
double    *h_error,   *d_error;

void init_param_host(){

  h_wet       = (int *)malloc(nxz*sizeof(int)); // wet/dry pointer
  h_dp        = (double *)malloc(nxz*sizeof(double)); // dynamic pressure
  h_u         = (double *)malloc(nxz*sizeof(double));
  h_un        = (double *)malloc(nxz*sizeof(double*)); // zonal speed
  h_ustar     = (double *)malloc(nxz*sizeof(double));
  h_w         = (double *)malloc(nxz*sizeof(double));
  h_wn        = (double *)malloc(nxz*sizeof(double)); // vertical speed
  h_wstar     = (double *)malloc(nxz*sizeof(double));
  h_q         = (double *)malloc((nx+2)*sizeof(double)); // depth-integrated flow
  h_depth     = (double *)malloc((nx+2)*sizeof(double));

  // matrices and parameters for S.O.R iteration on host
  h_ct        = (double *)malloc(nxz*sizeof(double));
  h_cb        = (double *)malloc(nxz*sizeof(double));
  h_ce        = (double *)malloc(nxz*sizeof(double));
  h_cw        = (double *)malloc(nxz*sizeof(double));
  h_ctot      = (double *)malloc(nxz*sizeof(double));
  h_pstar     = (double *)malloc(nxz*sizeof(double));
  h_error     = (double *)malloc(nxz*sizeof(double));
  h_dpstore   = (double *)malloc((nx+2)*sizeof(double));
}

void init_param_device(){
  // memory allocation
  HANDLE_ERROR(cudaMalloc((void**)&d_wet        , nxz*sizeof(h_wet[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_dp         , nxz*sizeof(h_dp[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_u          , nxz*sizeof(h_u[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_un         , nxz*sizeof(h_un[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_ustar      , nxz*sizeof(h_ustar[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_w          , nxz*sizeof(h_w[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_wn         , nxz*sizeof(h_wn[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_wstar      , nxz*sizeof(h_wstar[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_q          , (nx+2)*sizeof(h_q[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_depth      , (nx+2)*sizeof(h_depth[0])));

  // matrices and parameters for S.O.R iteration on device
  HANDLE_ERROR(cudaMalloc((void**)&d_ct         , nxz*sizeof(h_ct[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_cb         , nxz*sizeof(h_cb[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_cw         , nxz*sizeof(h_cw[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_ce         , nxz*sizeof(h_ce[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_ctot       , nxz*sizeof(h_ctot[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_pstar      , nxz*sizeof(h_pstar[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_error      , nxz*sizeof(h_error[0])));
  HANDLE_ERROR(cudaMalloc((void**)&d_dpstore    , (nx+2)*sizeof(h_dpstore[0])));

  // copy from host to device
  HANDLE_ERROR(cudaMemcpy(d_wet     , h_wet     , nxz*sizeof(d_wet[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_error   , h_error   , nxz*sizeof(h_error[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_dp      , h_dp      , nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_u       , h_u       , nxz*sizeof(h_u[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_un      , h_un      , nxz*sizeof(h_un[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_ustar   , h_ustar   , nxz*sizeof(h_ustar[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_w       , h_w       , nxz*sizeof(h_w[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_wn      , h_wn      , nxz*sizeof(h_wn[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_wstar   , h_wstar   , nxz*sizeof(h_wstar[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_q       , h_q       , (nx+2)*sizeof(h_q[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_depth   , h_depth   , (nx+2)*sizeof(h_depth[0]), cudaMemcpyHostToDevice));

  HANDLE_ERROR(cudaMemcpy(d_ct      , h_ct      , nxz*sizeof(h_ct[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_cb      , h_cb      , nxz*sizeof(h_cb[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_ce      , h_ce      , nxz*sizeof(h_ce[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_cw      , h_cw      , nxz*sizeof(h_cw[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_pstar   , h_pstar   , nxz*sizeof(h_pstar[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_ctot    , h_ctot    , nxz*sizeof(h_ctot[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_dpstore , h_dpstore , nx *sizeof(h_dpstore[0]), cudaMemcpyHostToDevice));
}
