// init
__global__ void initWet(int *wet){
  int i = threadIdx.x + blockIdx.x * blockDim.x;
  wet[i] = 1;
}

__global__ void initWetFalse(int *wet){
    int lBound    = blockIdx.x * (d_nx+1);
    int rBound    = lBound + d_nx;
    wet[lBound]   = 0;
    wet[rBound]   = 0;
}

__global__ void initBottomTopography(double* depth){
  if(threadIdx.x <= 50){
    depth[threadIdx.x+1] = 100.0-95.0*(double)(threadIdx.x+1)/(double)51;
  }else{
    depth[threadIdx.x+1] = 5.0+95.0*(double)((threadIdx.x+1)-51)/(double)(d_nx-51);
  }
}

__global__ void initWetAndDryPointer(double *depth, int *wet){
  for(int i=2; i<nz-1; i++){
    int nb = (int)(depth[i+1]/dz);
    for(int j=(nb+30); j< nx/*nz+1*/; j++){
      int idx = j + (i+1)*(nx+1);
      wet[idx] = false;
    }
  }
}

__global__ void initParamSOR(double *ct, double *cb, double *ce, double *cw, double *ctot, int *wet){
    int idx      = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
    int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (d_nx+1);
    int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
    int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (d_nx+1);
    int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (d_nx+1);

    int lBound = (blockIdx.x+1) * (d_nx+1);
    int rBound = lBound + d_nx;

    if((idx != lBound) && (idx != rBound)){
      ct[idx] = (double)(d_dx/d_dz);
      cb[idx] = (double)(d_dx/d_dz);
      ce[idx] = (double)(d_dz/d_dx);
      cw[idx] = (double)(d_dz/d_dx);
      if(!wet[idxZmin]){
        ct[idx] = 0.0;
      }
      if(!wet[idxZplus]){
        cb[idx] = 0.0;
      }
      if(!wet[idxXplus]){
        ce[idx] = 0.0;
      }
      if(!wet[idxXmin]){
        cw[idx] = 0.0;
      }
      ctot[idx] = ct[idx] + cb[idx] + ce[idx] + cw[idx];
    }
}

// dyn
__global__ void adCreateData(double *dp, double t){
  dp[1] = 1.0*sin(2.0*3.14*t/8.0)*1028.0*9.81;
}
__global__ void surfacePressureField(double *dpstore, double *dp){
    dpstore[blockIdx.x] = dp[blockIdx.x];
}

__global__ void calculateUstarAndWstar(double *ustar, double *u, double *wstar, double *w, double *dp, int *wet){
  d_drdxh = 0.5/(d_RHO*d_dx);
  d_drdzh = 0.5/(d_RHO*d_dz);
  int idx      = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
  int idxXplus = (threadIdx.x+1) + ((blockIdx.x+1)) * (d_nx+1);


  int lBound = (blockIdx.x+1) * (d_nx+1);
  int rBound = lBound + blockDim.x;

  if((idx != lBound) && (idx != rBound)){
    if(wet[idx]){
      double pressx = -d_drdxh*(dp[idxXplus] - dp[idx]);
      if(wet[idxXplus]) {
        ustar[idx] = u[idx] + d_dt*pressx;
      }
      double pressz = -d_drdzh*(dp[idxZmin] - dp[idx]);
      if(wet[idxZmin]){
        wstar[idx] = w[idx] + d_dt*pressz;
      }
    }
  }

}
__global__ void calculatePoisson(double *pstar, double *ustar, double *u, double *wstar, double *w){
  int idx      = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  int idxZplus = threadIdx.x + ((blockIdx.x+1)+1) * (d_nx+1);
  int idxXmin  = (threadIdx.x-1) + ((blockIdx.x+1)) * (d_nx+1);

  int lBound = (blockIdx.x+1) * (d_nx+1);
  int rBound = lBound + d_nx;

  if((idx != lBound) && (idx != rBound)){
    pstar[idx] = -2.0*d_RHO/d_dt * \
      ((ustar[idx] - u[idx] - ustar[idxXmin] + u[idxXmin]) * d_dz + \
      (wstar[idx] - w[idx] - wstar[idxZplus] + w[idxZplus]) * d_dx);
  }
}

// S.O.R Algorithm
__global__ void red_SOR(double *ct, double *cb, double *ce, double *cw, double *ctot, double *pstar, double *dp, int *wet, double *error){
  int idx, lBound, rBound, hor;
  int idxZplus, idxZmin, idxXplus, idxXmin;
  double p1,p2,term1;

  if((blockIdx.x % 2) == 0){
    hor = 2 * threadIdx.x + 1;
  }else{
    hor = 2 * threadIdx.x;
  }

  idx    = hor + (blockIdx.x+1)*(d_nx+1);
  lBound = (blockIdx.x+1) * (d_nx+1);
  rBound = lBound + d_nx;
  if((idx != lBound) && (idx != rBound)){
    idxZplus = hor + ((blockIdx.x+1)+1) * (d_nx+1);
    idxZmin  = hor + ((blockIdx.x+1)-1) * (d_nx+1);
    idxXplus = (hor+1) + (blockIdx.x+1) * (d_nx+1);
    idxXmin  = (hor-1) + (blockIdx.x+1) * (d_nx+1);
    if(wet[idx]){
      error[idx] = 0.0;
      p1 = dp[idx];
      term1 = pstar[idx] + \
      (ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus]) + \
      (cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus]);
      p2 = (1.0 - d_omega) * p1 + d_omega * term1 / ctot[idx];
      dp[idx] = p2;
      error[idx] = fabs(p2 - p1);
    }
  }
}

__global__ void black_SOR(double *ct, double *cb, double *ce, double *cw, double *ctot, double *pstar, double *dp, int *wet, double *error){
  int idx, lBound, rBound, hor;
  int idxZplus, idxZmin, idxXplus, idxXmin;
  double p1,p2,term1;

  if((blockIdx.x % 2) != 0){
    hor = 2 * threadIdx.x + 1;
  }else{
    hor = 2 * threadIdx.x;
  }

  idx    = hor + (blockIdx.x+1)*(d_nx+1);
  lBound = (blockIdx.x+1) * (d_nx+1);
  rBound = lBound + d_nx;
  if((idx != lBound) && (idx != rBound)){
    idxZplus = hor + ((blockIdx.x+1)+1) * (d_nx+1);
    idxZmin  = hor + ((blockIdx.x+1)-1) * (d_nx+1);
    idxXplus = (hor+1) + (blockIdx.x+1) * (d_nx+1);
    idxXmin  = (hor-1) + (blockIdx.x+1) * (d_nx+1);
    if(wet[idx]){
      error[idx] = 0.0;
      p1 = dp[idx];
      term1 = pstar[idx] + \
      (ct[idx] * dp[idxZmin] + cb[idx] * dp[idxZplus]) + \
      (cw[idx] * dp[idxXmin] + ce[idx] * dp[idxXplus]);
      p2 = (1.0 - d_omega) * p1 + d_omega * term1 / ctot[idx];
      dp[idx] = p2;
      error[idx] = fabs(p2 - p1);
    }
  }
}

__global__ void boundaryLeftRight(double *dp){
  int lBound, rBound;
  lBound = (blockIdx.x+1)*(d_nx+1);
  rBound = lBound + nx;
  dp[lBound] = dp[lBound + 1];
  dp[rBound] = dp[rBound - 1];
}

__global__ void predictNewVelocity(double *un, double *ustar, double *wn, double *wstar, double *dp, int *wet){
  int idx    = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  int lBound = (blockIdx.x+1) * (d_nx+1);
  int rBound = lBound + d_nx;
  int idxZmin  = threadIdx.x + ((blockIdx.x+1)-1) * (d_nx+1);
  int idxXplus = (threadIdx.x+1) + (blockIdx.x+1) * (d_nx+1);
  double pressz, pressx;

  if((idx != lBound) && (idx != rBound)){
    if(wet[idx]){
      pressx = -d_drdxh*(dp[idxXplus] - dp[idx]);
      pressz = -d_drdzh*(dp[idxZmin] - dp[idx]);
      if(wet[idxXplus]){
        un[idx] = ustar[idx] + d_dt * pressx;
      }
      if(wet[idxZmin]){
        wn[idx] = wstar[idx] + d_dt*pressz;
      }
    }
  }
}

__global__ void predictDepthIntegratedFlow(double *q, double *un){
  q[blockIdx.x] = 0.0;
  for(int j = 0; j<nz-1; j++){
    int idx  = blockIdx.x + (j+1)*(nx+1);
    q[blockIdx.x] = q[blockIdx.x] + d_dz*un[idx];
  }
}

__global__ void predictSurfacePressureField(double *dp, double *dpstore, double *q){
  dp[blockIdx.x+1] = dpstore[blockIdx.x+1] - d_dt * d_RHO * d_G * (q[(blockIdx.x+1)] - q[blockIdx.x]) / d_dx;
}

__global__ void lateralBoundaryConditionQ(double *q){
    q[0] = 0.0;
    q[d_nx+1] = 0.0;
    q[d_nx] = q[d_nx-1];
}

__global__ void updatingNextTimeStep(double *u, double *un, double *w, double *wn){
  int idx = threadIdx.x + (blockIdx.x+1) * (d_nx+1);
  u[idx]  = un[idx];
  w[idx]  = wn[idx];
}

__global__ void lateralBoundaryCondition(double *u){
  u[blockIdx.x*d_nx+1] = 0.0;
  u[blockIdx.x*d_nx+d_nx-1] = 0.0;
  u[blockIdx.x*d_nx+d_nx] = 0.0;
}
