#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(X, Y) (((X) < (Y)) ? (X) : (Y))

#define G 9.81 // acceleration due to gravity
#define RHO 1028.0 // reference density
#define PI 3.1416 // pi

#define nx 511 // horizontal dimension
#define nz 255 // vertical dimension

// grid parameters
double dt = 0.05, dx = 5.0, dz = 2.0, ad;
int i,j;
// forcing parameters
double amplitude = 1.0;
double period    = 8.0;

// coefficients for SOR
double    omega = 1.4;
double    peps = 1.e-2;
int here = 0;
bool**     wet;
double**   dp;
double**   u;
double**   un;
double**   w;
double**   wn;
double**   ustar;
double**   wstar;
double**   pstar;
double**   ct;
double**   cb;
double**   ce;
double**   cw;
double**   ctot;
double*   depth;
double*   q;

void init_param(){

  dp    = (double **)malloc((nz+1)*sizeof(double*)); // dynamic pressure
  u     = (double **)malloc((nz+1)*sizeof(double*));
  un    = (double **)malloc((nz+1)*sizeof(double*)); // zonal speed
  ustar = (double **)malloc((nz+1)*sizeof(double*));
  w     = (double **)malloc((nz+1)*sizeof(double*));
  wn    = (double **)malloc((nz+1)*sizeof(double*)); // vertical speed
  wstar = (double **)malloc((nz+1)*sizeof(double*));
  wet   = (bool **)malloc((nz+1)*sizeof(bool*)); // wet/dry pointer
  for(i=0;i<=nz+1;i++){
    dp[i]     = (double *)malloc((nx+1)*sizeof(double*));
    u[i]      = (double *)malloc((nx+1)*sizeof(double*));
    un[i]     = (double *)malloc((nx+1)*sizeof(double*)); // zonal speed
    ustar[i]  = (double *)malloc((nx+1)*sizeof(double*));
    w[i]      = (double *)malloc((nx+1)*sizeof(double*));
    wn[i]     = (double *)malloc((nx+1)*sizeof(double*)); // vertical speed
    wstar[i]  = (double *)malloc((nx+1)*sizeof(double*));
    wet[i]    = (bool *)malloc((nx+1)*sizeof(bool*)); // wet/dry pointer
  }
  q = (double *)malloc((nx+1)*sizeof(double)); // depth-integrated flow
  depth = (double *)malloc((nx+1)*sizeof(double));

  // matrices and parameters for S.O.R iteration
  ct    = (double **)malloc((nz+1)*sizeof(double*));
  cb    = (double **)malloc((nz+1)*sizeof(double*));
  ce    = (double **)malloc((nz+1)*sizeof(double*));
  cw    = (double **)malloc((nz+1)*sizeof(double*));
  ctot  = (double **)malloc((nz+1)*sizeof(double*));
  for(i=1; i<=nz; i++){
    ct[i]    = (double *)malloc((nx+1)*sizeof(double));
    cb[i]    = (double *)malloc((nx+1)*sizeof(double));
    ce[i]    = (double *)malloc((nx+1)*sizeof(double));
    cw[i]    = (double *)malloc((nx+1)*sizeof(double));
    ctot[i]  = (double *)malloc((nx+1)*sizeof(double));
  }

  pstar = (double **)malloc((nz+1)*sizeof(double*));
  for(i=1; i<=nz; i++){
    pstar[i] = (double *)malloc((nx+1)*sizeof(double));
  }
}
