#include <stdio.h>
#include <stdlib.h>
#define nEta1  12900
#define nEta   129
#define nDp1   848500
#define nDp    8385
#define nTime  100
#define nx     64
#define nz     128

int main(){
  // int nEta1 = (nz-1)*100;
  // int nEta  = nz-1;
  // int nDp1  = (nx-1)*nEta + 10000;
  // int nDp   = (nx-1)*(nz-1);

  float *eta1,
        *eta,
        *dp1;
  float dp[(nz-1)][(nx-1)];
  eta1  = (float*)malloc(nEta1*sizeof(float));
  eta   = (float*)malloc(nEta*sizeof(float));
  dp1   = (float*)malloc(nDp1*sizeof(float));
  int count = 0,
      iPlot = 0,
      itop  = 0,
      ibot  = 0,
      t     = 0,
      x     = 0,
      i     = 0,
      j     = 0;
  FILE *etaDat = fopen("eta.dat", "r");
  FILE *dpDat = fopen("dp.dat", "r");

  if(!etaDat){
    printf("Error: eta.dat tidak dapat dibaca!\n");
  }else{
    printf("File eta.dat berhasil dibuka\n");
  }

  if(!dpDat){
    printf("Error: dp.dat tidak dapat dibaca!\n");
  }else{
    printf("File dp.dat berhasil dibuka\n");
  }

  count = 0;
  while(!feof(etaDat)){
    fscanf(etaDat, "%f", &eta1[count]);
    count++;
  }
  count = 0;
  while(!feof(dpDat)){
    fscanf(dpDat, "%f", &dp1[count]);
    count++;
  }

  FILE *gnuplot = popen("gnuplot", "w");
  if(gnuplot == NULL){
    printf("GNU Plot tidak dapat dibuka\n");
  }else{
    fprintf(gnuplot, "reset\n");
    fprintf(gnuplot, "set xrange[0:600]\n");
    fprintf(gnuplot, "set yrange[-40:10]\n");
    fprintf(gnuplot, "set ylabel 'z' rotate by 0\n");
    fprintf(gnuplot, "set xlabel 'x'\n");
    // fprintf(gnuplot, "set terminal gif truecolor enhanced\n");
    // animation loop
    for(t = 0; t< 100; t++){
      fprintf(gnuplot, "set title 'Time = %i'\n",t);
      // mengisi nilai dp
      itop = (t)*(nx);
      ibot = itop+(nx-2);
      for(i=0;i<=(nz-2);i++){
        for(j=0;j<=(nx-2);j++){
          dp[i][j] = dp1[i+j*(nz-1)+t*nDp];
        }
      }
      // mengisi nilai eta
      for(i=0;i<=(nx-2);i++){
        eta[i] = eta1[i+t*(nz-1)];
      }

      // plot data
      fprintf(gnuplot, "plot '-' with lines lw 2 lc rgb '#a8c6f7' notitle ");
      for(i=0;i<=19;i++){
        fprintf(gnuplot, ",'-' with lines lw 2 lc rgb '#a8c6f7' notitle ");
      }
      fprintf(gnuplot, ",0.44*x-170 w filledcurve below x1 lt 1 lc rgb \"#33c68943\" t ''");
      fprintf(gnuplot, "\n");
      x = 0;

      for(iPlot=0;iPlot<=20;iPlot++){
        x = 0;
          for(i=0;i<=(nz-2);i++){
            fprintf(gnuplot, "%i %0.10f\n", x, 5*dp[i][iPlot]-iPlot*2);
            x+=5;
          }
          fprintf(gnuplot, "end\n");
      }
      // fprintf(gnuplot, "set output 'gif/output_%i.gif'\n", t);
      fprintf(gnuplot, "pause 0.01\n");
    }
  }
  pclose(gnuplot);

  free(eta1);
  free(eta);
  free(dp1);
  // free(dp);
  fclose(etaDat);
  fclose(dpDat);
  return 0;
}
