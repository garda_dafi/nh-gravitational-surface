#include "sub_kernel.cu"
#include "../find_maximum.c"

void init(){
  dim3 gridWet(nz+1);
  dim3 blockWet(nx+2);
  initWet<<<gridWet,blockWet>>>(d_wet);
  cudaDeviceSynchronize();

  dim3 gridWetFalse(nz);
  dim3 blockWetFalse(1);
  initWetFalse<<<gridWetFalse,blockWetFalse>>>(d_wet);
  cudaDeviceSynchronize();

  dim3 gridBottomTopograhy(1);
  dim3 blockBottomTopograhy(nx-2);
  initBottomTopography<<<gridBottomTopograhy, blockBottomTopograhy>>>(d_depth);
  cudaDeviceSynchronize();



  dim3 gridWetAndDry(1);
  dim3 blockWetAndDry(1);
  // initWetAndDryPointer<<<gridWetAndDry, blockWetAndDry>>>(d_depth, d_wet);
  // Synchronize init data
  cudaDeviceSynchronize();
  dim3 gridParamSOR(nz-1);
  dim3 blockParamSOR(nx);
  initParamSOR<<<gridParamSOR, blockParamSOR>>>(d_ct, d_cb, d_ce, d_cw, d_ctot, d_wet);
  cudaDeviceSynchronize();
}

void dyn(){
  int __attribute__((unused)) nsor, nstop, perr;

  int i        , j       , idx      , lBound, rBound,
     idxZplus , idxZmin , idxXplus , idxXmin;
 double h_p1, h_p2, h_term1;

  HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(d_dp[0]), cudaMemcpyDeviceToHost));
  // seal-level forcing
  h_dp[1] = h_ad*RHO*G;
  HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));

  // Surface pressure field
  dim3 gridSurfacePressureField(nx+1);
  dim3 blockSurfacePressureField(1);
  surfacePressureField<<<gridSurfacePressureField, blockSurfacePressureField>>>(d_dpstore, d_dp);
  cudaDeviceSynchronize();

  // calculate ustar and wstar
  dim3 gridUstarAndWstar(nz-1);
  dim3 blockUstarAndWstar(nx);
  calculateUstarAndWstar<<<gridUstarAndWstar, blockUstarAndWstar>>>(d_ustar, d_u, d_wstar, d_w, d_dp, d_wet);
  cudaDeviceSynchronize();
  HANDLE_ERROR(cudaMemcpy(h_wstar, d_wstar, nxz*sizeof(d_wstar[0]), cudaMemcpyDeviceToHost));
  for(int i=0;i<nz;i++){
    for(int j=90; j<nx; j++){
      int idx = j+i*(nx);
      printf("%0.7e ", h_wstar[idx]);
    }
    printf("\n");
  }
  // Step 3: calculate right-hand side of poisson equation
  dim3 gridCalculatePoisson(nz-1);
  dim3 blockCalculatePoisson(nx);
  calculatePoisson<<<gridCalculatePoisson, blockCalculatePoisson>>>(d_pstar,d_ustar, d_u, d_wstar, d_w);
  cudaDeviceSynchronize();
  HANDLE_ERROR(cudaMemcpy(h_pstar, d_pstar, nxz*sizeof(h_dp[0]), cudaMemcpyDeviceToHost));
  // Synchronize data

  for(i = 0; i<nz-1; i++){
    for(j = 0; j<nx; j++){
      idx    = j + (i+1)*nx;
      lBound = (i+1) * nx;
      rBound = lBound + (nx-1);
      if((idx != lBound) && (idx != rBound)){
        idxZplus = j + ((i+1)+1) * nx;
        idxZmin  = j + ((i+1)-1) * nx;
        idxXplus = (j+1) + (i+1) * nx;
        idxXmin  = (j-1) + (i+1) * nx;
        if(h_wet[idx]){
          h_p1 = h_dp[idx];
          h_term1 = h_pstar[idx] + \
          (h_ct[idx] * h_dp[idxZmin] + h_cb[idx] * h_dp[idxZplus]) + \
          (h_cw[idx] * h_dp[idxXmin] + h_ce[idx] * h_dp[idxXplus]);
          h_p2 = (1.0 - omega) * h_p1 + omega * h_term1 / h_ctot[idx];
          h_dp[idx] = h_p2;
          // printf("%0.2f ", h_dp[idx]);
          h_error[idx] = fabs(h_p2 - h_p1);
        } // endif
      }
    }
    // printf("\n");
  }


  for(i=0; i<nz-1; i++){
    lBound = (i+1)*nx;
    rBound = lBound + (nx-1);
    h_dp[lBound] = h_dp[lBound + 1];
    h_dp[rBound] = h_dp[rBound - 1];
  }

  HANDLE_ERROR(cudaMemcpy(d_dp, h_dp, nxz*sizeof(h_dp[0]), cudaMemcpyHostToDevice));
  HANDLE_ERROR(cudaMemcpy(d_error, h_error, nxz*sizeof(h_error[0]), cudaMemcpyHostToDevice));

  // Step 4: S.O.R Iteration
  nstop = 500;
  for(nsor = 1; nsor<= nstop; nsor++){
    // Step 4.1: S.O.R Solver
    red_SOR<<< nz-1,(nx/2) >>>(
      d_ct, d_cb, d_ce, d_cw, d_ctot,
      d_pstar, d_dp, d_wet, d_error);
    cudaDeviceSynchronize();
    black_SOR<<< nz-1,(nx/2) >>>(
      d_ct, d_cb, d_ce, d_cw, d_ctot,
      d_pstar, d_dp, d_wet, d_error);
    cudaDeviceSynchronize();


    boundaryLeftRight<<< nz-1,1 >>>(d_dp);
    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaMemcpy(h_dp, d_dp, nxz*sizeof(h_dp[0]), cudaMemcpyDeviceToHost));

    // Step 4.2: Predict Wew Velocities
    dim3 gridPredictNewVelocity(nz-1);
    dim3 blockPredictNewVelocity(nx);
    predictNewVelocity<<< gridPredictNewVelocity, blockPredictNewVelocity >>>(d_un, d_ustar, d_wn, d_wstar, d_dp, d_wet);
    cudaDeviceSynchronize();

    // Step 4.3: Predict Depth-integrated flow
    dim3 gridPredictDepthIntegratedFlow(nx);
    dim3 blockPredictDepthIntegratedFlow(1);
    predictDepthIntegratedFlow<<< gridPredictDepthIntegratedFlow, blockPredictDepthIntegratedFlow >>>(d_q, d_un);
    cudaDeviceSynchronize();

    // Lateral boundary condition
    lateralBoundaryConditionQ<<<1,1>>>(d_q);
    cudaDeviceSynchronize();
    // HANDLE_ERROR(cudaMemcpy(h_q, d_q, (nx+2)*sizeof(d_q[0]), cudaMemcpyDeviceToHost));
    // for(int i = 0; i<nx;i++){
    //   printf("%0.7e ", h_q[i]);
    //   printf("\n");
    // }
    // step 4.4: predict surface pressure field
    predictSurfacePressureField<<<nx-1,1>>>(d_dp, d_dpstore, d_q);
    cudaDeviceSynchronize();
    HANDLE_ERROR(cudaMemcpy(h_error, d_error, nxz*sizeof(h_error[0]), cudaMemcpyDeviceToHost));
    if(find_maximum(h_error, nxz) <= h_peps){
      printf("---------------------------------------------------------Nstop = %i Error = %0.7f\n",nsor, find_maximum(h_error, nxz));
      nstop = nsor;
    }
  }

  // Updating for next time step
  dim3 gridUpdatingNextTimeStep(nz-1);
  dim3 blockUpdatingNextTimeStep(nx);
  updatingNextTimeStep<<<gridUpdatingNextTimeStep, blockUpdatingNextTimeStep>>>(d_u, d_un, d_w, d_wn);
  cudaDeviceSynchronize();

  // Lateral boundary conditions
  dim3 gridLateralBoundaryCondition(nz+1);
  dim3 blockLateralBoundaryCondition(1);
  lateralBoundaryCondition<<<gridLateralBoundaryCondition, blockLateralBoundaryCondition>>>(d_u);
  cudaDeviceSynchronize();

} // end dyn
